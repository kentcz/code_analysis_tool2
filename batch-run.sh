#!/bin/bash

JAVA_CMD="java -jar target/CodeAnalysisTool-1.0-SNAPSHOT.jar" 
CONFIG="config.yaml"
BENCHMARK="XSBench"
DBFILE="/home/kentcz/research/proxy_apps/output/XSBench/hotspots/sqlite-db/dicer.db"
CA_ROOT_PATH="/home/kentcz/scratch/tmp_codeanalysis/"
KERNEL="XSBench_4210720_4210732"


if [ `hostname` == "vivace" ]
then
	CA_ROOT_PATH="/home/mic/scratch/new_analysis/ca_output/${BENCHMARK}/"
	DBFILE="/home/mic/proxy_apps/output/${BENCHMARK}/hotspots/sqlite-db/dicer.db"
	CONFIG="vivace_config.yaml"
fi

# Exit immeadiately if any command fails
#set -e

rm application.log
mvn clean package
if [[ $? -ne 0 ]]
then
	exit 1
fi

for BENCHMARK in LULESH MiniFE CoSP2 CoHMM CoMD CoEVP XSBench RSBench Nekbone Lassen UMT
do
	CA_ROOT_PATH="/home/mic/scratch/new_analysis/ca_output/${BENCHMARK}/"
	DBFILE="/home/mic/proxy_apps/output/${BENCHMARK}/hotspots/sqlite-db/dicer.db"
	mkdir -p ${CA_ROOT_PATH}
	echo ${JAVA_CMD} analyze-command -o ${CA_ROOT_PATH} -i ${DBFILE} ${CONFIG}
	${JAVA_CMD} analyze-command -o ${CA_ROOT_PATH} -i ${DBFILE} ${CONFIG}
done
