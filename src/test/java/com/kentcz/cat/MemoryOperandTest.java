package com.kentcz.cat;

import com.kentcz.cat.models.MemoryOperand;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MemoryOperandTest {
    @Test
    public void testBasicInstruction() {

        // ./xed-ex1 -64 c4 c1 7d 11 44 fd 00
        // vmovupd %ymm0,0x0(%r13,%rdi,8)
        // Example: "YMMWORD PTR [r13+rdi*8]"
        MemoryOperand memoryOperand = new MemoryOperand("MEM0",
                true, 64, "MEM", false, true, "R13", "RDI", 8, 0, 64, 256);
        assertEquals("YMMWORD PTR [r13+rdi*8]", memoryOperand.toAssembly());

    }
}
