package com.kentcz.cat;

import com.kentcz.cat.models.opcodemix.OpcodeSample;
import com.kentcz.cat.services.OpcodeMixParserService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OpcodeMixParserTest {
    @Test
    public void testParser() {
        OpcodeMixParserService parser = new OpcodeMixParserService();
        OpcodeSample sample = parser.parseLine(" 965 VPINSRD                      254                0");
        assertEquals(965, sample.getId());
        assertEquals("VPINSRD", sample.getOpcode());
        assertEquals(254, sample.getCount());
        assertEquals(0, sample.getPredicatedCount());

        parser.parseFile("/home/kentcz/scratch/opcodemix/proxy_apps/CoEVP/opcodemix.out");
    }
}
