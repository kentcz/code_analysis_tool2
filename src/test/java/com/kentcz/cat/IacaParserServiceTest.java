package com.kentcz.cat;

import com.kentcz.cat.models.iaca.IacaInstruction;
import com.kentcz.cat.services.IacaParserService;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class IacaParserServiceTest {
    @Before
    public void setup() {

    }

    @Test
    public void testParseBlockThroughputLine() {
        String line = "Block Throughput: 8.90 Cycles       Throughput Bottleneck: FrontEnd";
        assertEquals(IacaParserService.parseBlockThroughputLine(line), 8.90, .001);
    }

    @Test
    public void testAVXInstruction() {
        String line = "|   2    | 1.0       |     | 1.0   1.0 |           |     |     |     |     |    | vmulpd ymm15, ymm2, ymmword ptr [r15+rdi*8+0x20]";
        IacaInstruction instruction = IacaParserService.parseInstructionLine(line);
        assertEquals(instruction.getNumUops(),2);
        assertEquals(instruction.getAsm(), "vmulpd ymm15, ymm2, ymmword ptr [r15+rdi*8+0x20]");
        assertEquals(instruction.getPort0(), 1.0, .0001);
        assertEquals(instruction.getPort0D(), 0.0, .0001);
        assertEquals(instruction.getPort1(), 0.0, .0001);
        assertEquals(instruction.getPort2(), 1.0, .0001);
        assertEquals(instruction.getPort2D(), 1.0, .0001);
        assertEquals(instruction.getPort3(), 0.0, .0001);
        assertEquals(instruction.getPort3D(), 0.0, .0001);
        assertEquals(instruction.getPort4(), 0.0, .0001);
        assertEquals(instruction.getPort5(), 0.0, .0001);
        assertEquals(instruction.getPort6(), 0.0, .0001);
        assertEquals(instruction.getPort7(), 0.0, .0001);
    }

    @Test
    public void testFakeInstruction() {
        String line = "|   2    | 0.0  00.00| 1.0 | 2.0   2.2 |33.33  3.0 | 4.0 | 5.0 | 6.0 | 7.0 |    | vmulpd ymm15, ymm2, ymmword ptr [r15+rdi*8+0x20]";
        IacaInstruction instruction = IacaParserService.parseInstructionLine(line);
        assertEquals(instruction.getNumUops(),2);
        assertEquals(instruction.getAsm(), "vmulpd ymm15, ymm2, ymmword ptr [r15+rdi*8+0x20]");
        assertEquals(instruction.getPort0(), 0.0, .0001);
        assertEquals(instruction.getPort0D(), 0.0, .0001);
        assertEquals(instruction.getPort1(), 1.0, .0001);
        assertEquals(instruction.getPort2(), 2.0, .0001);
        assertEquals(instruction.getPort2D(), 2.2, .0001);
        assertEquals(instruction.getPort3(), 33.33, .0001);
        assertEquals(instruction.getPort3D(), 3.0, .0001);
        assertEquals(instruction.getPort4(), 4.0, .0001);
        assertEquals(instruction.getPort5(), 5.0, .0001);
        assertEquals(instruction.getPort6(), 6.0, .0001);
        assertEquals(instruction.getPort7(), 7.0, .0001);
    }

    @Test
    public void testCmpInstruction() {
        String line = "|   1    |           |     |           |           |     |     | 1.0 |     |    | cmp r8, r9";
        IacaInstruction instruction = IacaParserService.parseInstructionLine(line);
        assertEquals(instruction.getNumUops(),1);
        assertEquals(instruction.getAsm(), "cmp r8, r9");
        assertEquals(instruction.getPort0(), 0.0, .0001);
        assertEquals(instruction.getPort0D(), 0.0, .0001);
        assertEquals(instruction.getPort1(), 0.0, .0001);
        assertEquals(instruction.getPort2(), 0.0, .0001);
        assertEquals(instruction.getPort2D(), 0.0, .0001);
        assertEquals(instruction.getPort3(), 0.0, .0001);
        assertEquals(instruction.getPort3D(), 0.0, .0001);
        assertEquals(instruction.getPort4(), 0.0, .0001);
        assertEquals(instruction.getPort5(), 0.0, .0001);
        assertEquals(instruction.getPort6(), 1.0, .0001);
        assertEquals(instruction.getPort7(), 0.0, .0001);
    }

    @Test
    public void testBranchInstruction() {
        String line = "|   0F   |           |     |           |           |     |     |     |     |    | jb 0xffffffffffffff70";
        IacaInstruction instruction = IacaParserService.parseInstructionLine(line);
        assertEquals(instruction.getNumUops(),0);
        assertEquals(instruction.getAsm(), "jb 0xffffffffffffff70");
        assertEquals(instruction.getPort0(), 0.0, .0001);
        assertEquals(instruction.getPort0D(), 0.0, .0001);
        assertEquals(instruction.getPort1(), 0.0, .0001);
        assertEquals(instruction.getPort2(), 0.0, .0001);
        assertEquals(instruction.getPort2D(), 0.0, .0001);
        assertEquals(instruction.getPort3(), 0.0, .0001);
        assertEquals(instruction.getPort3D(), 0.0, .0001);
        assertEquals(instruction.getPort4(), 0.0, .0001);
        assertEquals(instruction.getPort5(), 0.0, .0001);
        assertEquals(instruction.getPort6(), 0.0, .0001);
        assertEquals(instruction.getPort7(), 0.0, .0001);
    }

    @Test
    public void testMovInstruction() {
        String line = "|   2    |           |     | 0.5       | 0.5       | 1.0 |     |     |     |    | vmovupd ymmword ptr [r13+rdi*8], ymm0";
        IacaInstruction instruction = IacaParserService.parseInstructionLine(line);
        assertEquals(instruction.getNumUops(),2);
        assertEquals(instruction.getAsm(), "vmovupd ymmword ptr [r13+rdi*8], ymm0");
        assertEquals(instruction.getPort0(), 0.0, .0001);
        assertEquals(instruction.getPort0D(), 0.0, .0001);
        assertEquals(instruction.getPort1(), 0.0, .0001);
        assertEquals(instruction.getPort2(), 0.5, .0001);
        assertEquals(instruction.getPort2D(), 0.0, .0001);
        assertEquals(instruction.getPort3(), 0.5, .0001);
        assertEquals(instruction.getPort3D(), 0.0, .0001);
        assertEquals(instruction.getPort4(), 1.0, .0001);
        assertEquals(instruction.getPort5(), 0.0, .0001);
        assertEquals(instruction.getPort6(), 0.0, .0001);
        assertEquals(instruction.getPort7(), 0.0, .0001);
    }
}
