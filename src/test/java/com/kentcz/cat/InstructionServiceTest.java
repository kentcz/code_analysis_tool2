package com.kentcz.cat;

import com.kentcz.cat.models.Instruction;
import com.kentcz.cat.services.InstructionService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InstructionServiceTest {
    @Test
    public void basicSanitizeTest() {
        String input = "mov    rcx,QWORD PTR [rip+0x2ae41a]        # 2f5d98 <_DYNAMIC+0xc18>";
        String expectedOutput = "mov rcx,QWORD PTR [rip+0x2ae41a]";

        InstructionService instructionService = new InstructionService(null);
        String output = instructionService.sanitize(input);
        assertEquals(expectedOutput, output);
    }

    @Test
    public void movabsSanitizeTest() {
        String input = "movabs rcx,0x200000005";
        String expectedOutput = "mov rcx,0x200000005";

        InstructionService instructionService = new InstructionService(null);
        String output = instructionService.sanitize(input);
        assertEquals(expectedOutput, output);
    }
}
