package com.kentcz.cat;

import com.kentcz.cat.models.iaca.DiagraphEdge;
import com.kentcz.cat.services.IacaGraphParserService;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class IacaGraphParserServiceTest {
    @Test
    public void testIsVertexLineTrue() {
        String line = "25 [label=\"25. jb 0xffffffffffffff70\", style=filled, color=cyan]";
        boolean isVertexLine = IacaGraphParserService.isVertexLine(line);
        assertEquals(true, isVertexLine);
    }

    @Test
    public void testIsVertexLineFalse() {
        String line = "24 -> 25";
        boolean isVertexLine = IacaGraphParserService.isVertexLine(line);
        assertEquals(false, isVertexLine);
    }

    @Test
    public void testIsEdgeLineFalse() {
        String line = "25 [label=\"25. jb 0xffffffffffffff70\", style=filled, color=cyan]";
        boolean isEdgeLine = IacaGraphParserService.isEdgeLine(line);
        assertEquals(false, isEdgeLine);
    }

    @Test
    public void testIsEdgeLineTrue() {
        String line = "24 -> 25";
        boolean isEdgeLine = IacaGraphParserService.isEdgeLine(line);
        assertEquals(true, isEdgeLine);
    }

    @Test
    public void testParseEdgeLine() {
        String line = "24 -> 25";
        DiagraphEdge edge = IacaGraphParserService.parseEdgeLine(line);
        assertEquals(25, edge.getChildId());
        assertEquals(24, edge.getParentId());
    }


}
