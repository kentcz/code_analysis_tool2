package com.kentcz.cat;

import com.kentcz.cat.models.hotspots.HotspotNode;
import com.kentcz.cat.resources.HotspotParser;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HotspotParserTest {

    private final String input1 = "    [Loop@0x408a1a in compute_rhs_._omp_fn.0]       bt.B                0.51%           0";
    private final String input2 = "      [Loop at line 46 in y_solve_._omp_fn.0]       bt.B                1.06%           2.464";
    private final String input3 = " [Outside any loop]                                 [Unknown]           100%            0";
    private final String input4 = "  [Loop at line 106 in __libc_start_main]           libc-2.15.so        24.99%          0";
    private final String input5 = "     [Loop@0xff80 in func@0xff20]                   libgomp.so.1.0.0    0.14%           0.332";

    @Test
    public void testInput1() {
        HotspotParser hotspotParser = new HotspotParser();
        HotspotNode node = hotspotParser.parseLine(input1);
        assertEquals(4, node.getLevel());
        assertEquals("[Loop@0x408a1a in compute_rhs_._omp_fn.0]", node.getLabel());
        assertEquals("bt.B", node.getModule());
        assertEquals(0.51, node.getTotalTime(), 0.00001);
        assertEquals(0.0, node.getSelfTime(), 0.00001);
    }

    @Test
    public void testInput2() {
        HotspotParser hotspotParser = new HotspotParser();
        HotspotNode node = hotspotParser.parseLine(input2);
        assertEquals(6, node.getLevel());
        assertEquals("[Loop at line 46 in y_solve_._omp_fn.0]", node.getLabel());
        assertEquals("bt.B", node.getModule());
        assertEquals(1.06, node.getTotalTime(), 0.00001);
        assertEquals(2.464, node.getSelfTime(), 0.00001);
    }
/**
    @Test
    public void testInput3() {
        HotspotParser hotspotParser = new HotspotParser();
        HotspotNode node = hotspotParser.parseLine(input3);
        assertEquals(null, node);
    }

    @Test
    public void testInput4() {
        HotspotParser hotspotParser = new HotspotParser();
        HotspotNode node = hotspotParser.parseLine(input4);
        assertEquals(null, node);
    }

    @Test
    public void testInput5() {
        HotspotParser hotspotParser = new HotspotParser();
        HotspotNode node = hotspotParser.parseLine(input5);
        assertEquals(null, node);
    **/
}
