package com.kentcz.cat;

import com.kentcz.cat.models.Instruction;
import com.kentcz.cat.models.xed.XedInstruction;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class XedInstructionTest {

    private static final String sample_jb_instruction = "{\n" +
            "\"Decode\": \"0f 82 eb f7 ff ff \",\n" +
            "\"Class\":\"JB\",\n" +
            "\"Category\":\"COND_BR\",\n" +
            "\"ISAExt\":\"BASE\",\n" +
            "\"ISASet\":\"I86\",\n" +
            "\"Operands\": [\n" +
            "\t{ \"OpNum\":0,\"Type\":\"RELBR\",\"Details\":\"-2069\",\"VIS\":\"EXPLICIT\",\"RW\":\"R\",\"Bits\":32,\"NumElem\":1,\"ElemSize\":32,\"ElemType\":\"INT\"},\n" +
            "\t{ \"OpNum\":1,\"Type\":\"REG0\",\"Details\":\"RIP\",\"VIS\":\"SUPPRESSED\",\"RW\":\"RW\",\"Bits\":64,\"NumElem\":1,\"ElemSize\":0,\"ElemType\":\"INT\"},\n" +
            "\t{ \"OpNum\":2,\"Type\":\"REG1\",\"Details\":\"RFLAGS\",\"VIS\":\"SUPPRESSED\",\"RW\":\"R\",\"Bits\":64,\"NumElem\":1,\"ElemSize\":0,\"ElemType\":\"INT\"}\n" +
            "],\n" +
            "\"MemoryOps\":[\n" +
            "],\n" +
            "\"Flag_read\": [\"cf\"],\n" +
            "\"Flag_write\": [],\n" +
            "\"MemopBytes\":0\n" +
            "}";


    // ./xed-ex1 -64 c4 43 4d 18 5c ff 28 01
    // vinsertf128 $0x1,0x28(%r15,%rdi,8),%ymm6,%ymm11
    // vinsertf128 ymm11, ymm6, XMMWORD PTR [40+r15+rdi*8], 1
    private static final String sample_add_instruction = "{\n" +
            "\"Decode\": \"83 85 68 fd ff ff 01 \",\n" +
            "\"Class\":\"ADD\",\n" +
            "\"Category\":\"BINARY\",\n" +
            "\"ISAExt\":\"BASE\",\n" +
            "\"ISASet\":\"I86\",\n" +
            "\"Operands\": [\n" +
            "\t{ \"OpNum\":0,\"Type\":\"MEM0\",\"Details\":\"MEM\",\"VIS\":\"EXPLICIT\",\"RW\":\"RW\",\"Bits\":32,\"NumElem\":1,\"ElemSize\":32,\"ElemType\":\"INT\"},\n" +
            "\t{ \"OpNum\":1,\"Type\":\"IMM0\",\"Details\":\"1\",\"VIS\":\"EXPLICIT\",\"RW\":\"R\",\"Bits\":8,\"NumElem\":1,\"ElemSize\":8,\"ElemType\":\"INT\"},\n" +
            "\t{ \"OpNum\":2,\"Type\":\"REG0\",\"Details\":\"RFLAGS\",\"VIS\":\"SUPPRESSED\",\"RW\":\"W\",\"Bits\":32,\"NumElem\":1,\"ElemSize\":0,\"ElemType\":\"INT\"}\n" +
            "],\n" +
            "\"MemoryOps\":[\n" +
            "\t{ \"MemOpNum\":0,\"RW\":\"RW\",\"Base\":\"RBP\",\"Displacement\":-664,\"AWidth\":64}\n" +
            "],\n" +
            "\"Flag_read\": [],\n" +
            "\"Flag_write\": [\"of\",\"sf\",\"zf\",\"af\",\"pf\",\"cf\"],\n" +
            "\"MemopBytes\":4\n" +
            "}";

    private static final String sampleVinsert = "{\n" +
            "\"Decode\": \"c4 43 4d 18 5c ff 28 01 \",\n" +
            "\"Class\":\"VINSERTF128\",\n" +
            "\"Category\":\"AVX\",\n" +
            "\"ISAExt\":\"AVX\",\n" +
            "\"ISASet\":\"AVX\",\n" +
            "\"Operands\": [\n" +
            "\t{ \"OpNum\":0,\"Type\":\"REG0\",\"Details\":\"YMM11\",\"VIS\":\"EXPLICIT\",\"RW\":\"W\",\"Bits\":256,\"NumElem\":4,\"ElemSize\":64,\"ElemType\":\"DOUBLE\"},\n" +
            "\t{ \"OpNum\":1,\"Type\":\"REG1\",\"Details\":\"YMM6\",\"VIS\":\"EXPLICIT\",\"RW\":\"R\",\"Bits\":256,\"NumElem\":4,\"ElemSize\":64,\"ElemType\":\"DOUBLE\"},\n" +
            "\t{ \"OpNum\":2,\"Type\":\"MEM0\",\"Details\":\"MEM\",\"VIS\":\"EXPLICIT\",\"RW\":\"R\",\"Bits\":128,\"NumElem\":2,\"ElemSize\":64,\"ElemType\":\"DOUBLE\"},\n" +
            "\t{ \"OpNum\":3,\"Type\":\"IMM0\",\"Details\":\"1\",\"VIS\":\"EXPLICIT\",\"RW\":\"R\",\"Bits\":8,\"NumElem\":1,\"ElemSize\":8,\"ElemType\":\"UINT\"}\n" +
            "],\n" +
            "\"MemoryOps\":[\n" +
            "\t{ \"MemOpNum\":0,\"RW\":\"R\",\"Base\":\"R15\",\"Index\":\"RDI\",\"Scale\":8,\"Displacement\":40,\"AWidth\":64}\n" +
            "],\n" +
            "\"Exceptions\":\"AVX_TYPE_6\",\n" +
            "\"MemopBytes\":16\n" +
            "}\n";


    @Test
    public void test_jb_instruction() throws Exception {
        XedInstruction xedInstruction = XedInstruction.parseString(sample_jb_instruction);
        assertEquals("0f 82 eb f7 ff ff ", xedInstruction.getDecode());
        assertEquals("JB", xedInstruction.getInstructionClass());
        assertEquals("COND_BR", xedInstruction.getCategory());
        assertEquals("BASE", xedInstruction.getIsaExt());
        assertEquals("I86", xedInstruction.getIsaSet());
        assertEquals(3, xedInstruction.getOperands().size());
        assertEquals(0, xedInstruction.getMemoryOps().size());
        assertEquals(1, xedInstruction.getFlagRead().size());
        assertEquals(0, xedInstruction.getFlagWrite().size());
        assertEquals(0, xedInstruction.getMemOperandBytes());
    }

    @Test
    public void test_toInstruction_jb() throws Exception {
        XedInstruction xedInstruction = XedInstruction.parseString(sample_jb_instruction);
        Instruction instruction = xedInstruction.toInstruction();
        assertEquals("jb", instruction.getOperation());
        assertEquals(1, instruction.getReadFlags().size());
        assertEquals(0, instruction.getWriteFlags().size());
    }


    @Test
    public void parseVinsert() throws Exception {
        XedInstruction xedInstruction = XedInstruction.parseString(sampleVinsert);
        assertEquals("VINSERTF128", xedInstruction.getInstructionClass());
        assertEquals("AVX", xedInstruction.getCategory());
        assertEquals("AVX", xedInstruction.getIsaExt());
        assertEquals("AVX", xedInstruction.getIsaSet());
        assertEquals(4, xedInstruction.getOperands().size());
        assertEquals(1, xedInstruction.getMemoryOps().size());
        assertEquals(0, xedInstruction.getFlagRead().size());
        assertEquals(0, xedInstruction.getFlagWrite().size());
        assertEquals(16, xedInstruction.getMemOperandBytes());
        assertEquals("AVX_TYPE_6", xedInstruction.getExceptions());
        System.out.println(xedInstruction);

    }
    @Test
    public void testVinsert() throws Exception {
        XedInstruction xedInstruction = XedInstruction.parseString(sampleVinsert);
        Instruction instruction = xedInstruction.toInstruction();
        assertEquals("vinsertf128", instruction.getOperation());
        assertEquals("vinsertf128 ymm11,ymm6,XMMWORD PTR [r15+rdi*8+0x28],0x1", instruction.toAssembly());
        System.out.println(instruction.toAssembly());
    }

}
