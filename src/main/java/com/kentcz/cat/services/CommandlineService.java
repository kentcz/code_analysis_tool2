package com.kentcz.cat.services;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.google.common.io.Closeables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class CommandlineService {

    private static final Logger LOG = LoggerFactory.getLogger(CommandlineService.class);

    @Inject
    public CommandlineService() { }

    public String execute(String cmd) {
        return execute(cmd, true);
    }

    public String execute(String cmd, boolean printOutput) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            LOG.info(">" + cmd);
            if (printOutput) { System.out.println(">" + cmd); }
            Process p  = Runtime.getRuntime().exec(new String[]{"bash", "-c", cmd});
            p.waitFor();
            //String content = CharStreams.toString(new InputStreamReader(p.getInputStream(), Charsets.UTF_8));
            //Closeables.closeQuietly(p.getInputStream());
            //System.out.println(content);

            BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";

            while ((line = b.readLine()) != null) {
                LOG.info(line);
                if (printOutput) { System.out.println(line); }
                stringBuilder.append(line);
            }

            BufferedReader errorReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            line = "";

            while ((line = errorReader.readLine()) != null) {
                System.err.println(line);
                LOG.error(line);
                stringBuilder.append(line);
            }

            return stringBuilder.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return stringBuilder.toString();
    }
}
