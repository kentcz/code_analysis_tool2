package com.kentcz.cat.services;

import com.google.common.collect.Lists;
import com.kentcz.cat.models.BlockLocation;
import com.kentcz.cat.models.vtune.VTuneBasicBlock;
import com.kentcz.cat.models.vtune.VTuneSourceLocation;

import javax.inject.Inject;
import java.io.File;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

public class VTuneParserService {

    String db_filename;
    Connection db_conn = null;

    @Inject
    public VTuneParserService(String filename) {
        db_filename = filename;

        // open database connection
        try {
            Class.forName("org.sqlite.JDBC");
            db_conn = DriverManager.getConnection("jdbc:sqlite:" + db_filename);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
        System.out.println("Opened Database");
    }

    public long numSamples() {
        long num_samples = 0;
        try {
            Statement stmt = db_conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT COUNT(*) as num_samples "
                    + "FROM dd_sample "
                    + "WHERE event_type = 1;");
            while (rs.next()) {
                num_samples = rs.getLong("num_samples");
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return num_samples;
    }


    public List<VTuneSourceLocation> getSourceLocations(int basicblock) {
        List<VTuneSourceLocation> locations = Lists.newArrayList();
        try {
            Statement stmt = db_conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT *, " +
                    "dd_source_location.rowid as source_loc, " +
                    "dd_function.full_name as func_fullname, " +
                    "dd_function.full_name as func_name, " +
                    "dd_source_file.name as src_name, " +
                    "dd_source_file.path as src_path " +
                    "FROM dd_source_location " +
                    "LEFT JOIN dd_source_file ON dd_source_location.src_file=dd_source_file.rowid " +
                    "LEFT JOIN dd_function ON dd_source_location.function=dd_function.rowid " +
                    "WHERE dd_source_location.rowid IN (SELECT src_loc FROM dd_code_location WHERE bb="+basicblock+")");
            while (rs.next()) {
                VTuneSourceLocation location = new VTuneSourceLocation( rs.getLong("source_loc"),
                        rs.getString("src_name"),
                        rs.getString("line"),
                        rs.getString("src_path"),
                        rs.getString("func_fullname"),
                        rs.getString("func_name"));
                locations.add(location);
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return locations;
    }

    public List<VTuneBasicBlock> getBasicBlocks() {
        List<VTuneBasicBlock> blocks = Lists.newArrayList();
        long numSamples = numSamples();
        try {
            Statement stmt = db_conn.createStatement();
            String sql = "SELECT * FROM ("
                    + "SELECT bblock, COUNT(*)/" + numSamples + ".0 as num_samples, "
                    + "start_display_address, asm_size, next_bb, jump_target_bb, branch_type, start_rva, "
                    + "last_instr_rva, bin_name, bin_path "
                    + "FROM ( "
                    + "SELECT start_display_address, dd_basic_block.size as asm_size, next_bb, "
                    + "jump_target_bb, branch_type, start_rva, last_instr_rva, bin_name, bin_path, "
                    + "dd_basic_block.rowid as bblock "
                    + "FROM dd_sample "
                    + "LEFT JOIN dd_sample_event_type ON dd_sample.event_type=dd_sample_event_type.rowid "
                    + "LEFT JOIN dd_callsite ON dd_sample.callsite=dd_callsite.rowid "
                    + "LEFT JOIN dd_code_location ON dd_callsite.code_loc=dd_code_location.rowid "
                    + "LEFT JOIN dd_basic_block ON dd_code_location.bb=dd_basic_block.rowid "
                    + "LEFT JOIN dd_module_segment ON dd_basic_block.mod_seg=dd_module_segment.rowid "
                    + "LEFT JOIN dd_module_file on dd_module_segment.mod_file=dd_module_file.rowid "
                    + "WHERE event_type = 1 "
                    + ") "
                    + "GROUP BY bblock "
                    + "ORDER BY num_samples DESC "
                    + ") "
                    + "WHERE num_samples > 0.01 "
                    + "AND start_display_address IS NOT NULL;";
            //System.out.println(sql);
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                VTuneBasicBlock vTuneBasicBlock = new VTuneBasicBlock(
                        rs.getInt("bblock"),
                        rs.getLong("start_rva"),
                        rs.getLong("last_instr_rva"),
                        rs.getLong("start_display_address"),
                        rs.getInt("asm_size"),
                        rs.getInt("jump_target_bb"),
                        rs.getInt("branch_type"),
                        rs.getDouble("num_samples"),
                        new String(rs.getString("bin_name")),
                        new String(rs.getString("bin_path")));
                //bb.printBlock();
                blocks.add(vTuneBasicBlock);
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (VTuneBasicBlock basicBlock : blocks) {
            basicBlock.setSources(getSourceLocations((int) basicBlock.getRowId()));
            System.out.println("found " + basicBlock.getSources().size() + " sources for " + basicBlock.getRowId());
        }


        return blocks;
    }

    public List<BlockLocation> extractLoopNestDetails(String loopName) {
        List<BlockLocation> blocks = Lists.newArrayList();
        try {
            Statement stmt = db_conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT dd_function_range.start_display_address as start_address,\n" +
                    "dd_function_instance.size as num_bytes, bin_name, bin_path\n" +
                    "FROM dd_function\n" +
                    "LEFT JOIN dd_function_instance ON dd_function.rowid=dd_function_instance.function\n" +
                    "LEFT JOIN dd_function_range ON dd_function_instance.head_range=dd_function_range.rowid\n" +
                    "LEFT JOIN dd_module_segment ON dd_function_instance.mod_seg=dd_module_segment.rowid\n" +
                    "LEFT JOIN dd_module_file on dd_module_segment.mod_file=dd_module_file.rowid\n" +
                    "WHERE name='"+ loopName + "'");
            while (rs.next()) {
                blocks.add(new BlockLocation(rs.getString("bin_path"),
                        rs.getString("bin_name"),
                        rs.getLong("start_address"),
                        rs.getInt("num_bytes")));
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return blocks;
    }

    /**
    public void exportBasicBlocks(String outfile) {
        try {
            PrintStream out = new PrintStream(new File(outfile));
            exportBasicBlocks(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exportBasicBlocks(PrintStream out) {
        try {
            List<VTuneBasicBlock> blocks = getBasicBlocks();
            for (VTuneBasicBlock b : blocks) {
                out.println(b);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     **/

    public void exportBasicBlocks(String dir) {
        List<VTuneBasicBlock> basicBlocks = getBasicBlocks();
        exportBasicBlocksSummary(dir, basicBlocks);
        for (VTuneBasicBlock basicBlock : basicBlocks) {
            exportBasicBlock(dir, basicBlock);
        }
    }

    private void exportBasicBlocksSummary(String dir, List<VTuneBasicBlock> basicBlocks) {
        String outfile = String.format("%s/BasicBlocksSummary.out", dir);
        try {
            PrintStream out = new PrintStream(new File(outfile));
            for (VTuneBasicBlock basicBlock : basicBlocks) {
                long rowId = basicBlock.getRowId();
                double percent = basicBlock.getPercentTime();
                out.println(String.format("%s,%s, %s", rowId, percent, basicBlock.getDisplayAddress()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void exportBasicBlock(String dir, VTuneBasicBlock basicBlock) {
        String outfile = String.format("%s/BasicBlockDetails%s.out", dir,
                Long.toString(basicBlock.getRowId()));
        try {
            PrintStream out = new PrintStream(new File(outfile));
            out.println(basicBlock.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String branch_type_str(int branch_type) {
        if (branch_type == 0) { return "None"; }
        if (branch_type == 3) { return "Call"; }
        if (branch_type == 1) { return "Cond"; }
        if (branch_type == 4) { return "Ret"; }
        if (branch_type == 2) { return "Uncond"; }
        return "null";
    }
}
