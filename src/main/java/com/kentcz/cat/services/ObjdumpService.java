package com.kentcz.cat.services;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import com.kentcz.cat.configurations.ObjdumpConfiguration;
import com.kentcz.cat.models.objdump.ObjdumpInstruction;

import javax.inject.Inject;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ObjdumpService {

    private final CommandlineService commandlineService;
    private final String binPath;

    @Inject
    public ObjdumpService(ObjdumpConfiguration objdumpConfiguration, CommandlineService commandlineService) {
        this.binPath = objdumpConfiguration.getBinPath();
        this.commandlineService = commandlineService;
    }

    public void dumpBinary(String binaryFile, String outputFile) {
        String cmd = binPath + " -M intel --insn-width=16 -d " + binaryFile + " > " + outputFile;
        commandlineService.execute(cmd);
    }

    public List<ObjdumpInstruction> extractBlockFromBinary(String binaryFile, String temporaryFile, long startAddress, long stopAddress) {
        dumpBinary(binaryFile, temporaryFile);
        return extractBlockFromDump(temporaryFile, startAddress, stopAddress);
    }

    public void exportLines(String objdumpFile, String outputFile, long startAddress, long stopAddress) {
        try {
            PrintStream out = new PrintStream(new File(outputFile));
            BufferedReader fileBuffer = new BufferedReader(new FileReader(objdumpFile));

            String line;
            while ((line = fileBuffer.readLine()) != null) {
                if (line.length() >= 60 && line.charAt(8) == ':') {
                    String address_str = line.substring(0, 8).trim();
                    try {
                        long address = Long.parseLong(address_str, 16);
                        if (address >= startAddress && address < stopAddress) {
                            out.println(line);
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<ObjdumpInstruction> extractBlockFromDump(String objdumpFile, long startAddress, long stopAddress) {
        List<ObjdumpInstruction> instructions = new ArrayList<>();
        try {
            BufferedReader filebuffer = new BufferedReader(new FileReader(objdumpFile));

            String line;
            while ((line = filebuffer.readLine()) != null) {

                if (line.length() >= 60 && line.charAt(8) == ':') {

                    String address_str = line.substring(0, 8).trim();
                    String hex_str = line.substring(9, 58).trim();
                    String asm_str = line.substring(59).trim();

                    // Get Address
                    long address = 0;
                    try {
                        address = Long.parseLong(address_str, 16);
                    } catch (NumberFormatException e) {
                        // Ignore
                    }

                    if (address >= startAddress && address < stopAddress) {
                        instructions.add(new ObjdumpInstruction(address, hex_str, asm_str));
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return instructions;
    }

    public List<ObjdumpInstruction> extractBlockFromDump(String objdumpFile) {
        List<ObjdumpInstruction> instructions = new ArrayList<>();
        try {
            BufferedReader fileBuffer = new BufferedReader(new FileReader(objdumpFile));
            String line;
            while ((line = fileBuffer.readLine()) != null) {

                if (line.length() >= 60 && line.charAt(8) == ':') {

                    String address_str = line.substring(0, 8).trim();
                    String hex_str = line.substring(9, 58).trim();
                    String asm_str = line.substring(59).trim();

                    try {
                        long address = Long.parseLong(address_str, 16);
                        instructions.add(new ObjdumpInstruction(address, hex_str, asm_str));
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return instructions;
    }

    public List<ObjdumpInstruction> extractBlockFromIacaDump(String objdumpFile) {
        List<ObjdumpInstruction> instructions = new ArrayList<>();
        try {
            List<String> lines = Files.readLines(new File(objdumpFile), Charsets.UTF_8);
            System.out.println("Read " + lines.size() + " lines from " + objdumpFile);

            // Track code region
            boolean afterStartMarker = false;
            boolean beforeStopMarker = true;

            for (int lineNum = 0; lineNum < lines.size(); lineNum++) {
                String line = lines.get(lineNum);
                int colonIndex = line.indexOf(':');

                if (line.length() >= 60 && colonIndex < 9) {
                    System.out.println("line: " + line);

                    String address_str = line.substring(0, colonIndex).trim();
                    String hex_str = line.substring(colonIndex+1, colonIndex + 50).trim();
                    String asm_str = line.substring(colonIndex + 51).trim();

                    try {
                        long address = Long.parseLong(address_str, 16);
                        if (afterStartMarker && beforeStopMarker) {
                            instructions.add(new ObjdumpInstruction(address, hex_str, asm_str));
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }

                    System.out.println("instruction: " + asm_str);
                    if (isStartMarker(lines, lineNum)) {
                        afterStartMarker = true;
                    }

                    if (isEndMarker(lines, lineNum)) {
                        beforeStopMarker = false;
                    }
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return instructions;
    }

    private boolean isEndMarker(List<String> lines, int index) {
        return lines.size() > index + 3
                && lines.get(index).contains("jb")
                && lines.get(index+1).contains("bb de 00 00 00")
                && lines.get(index+2).contains("64")
                && lines.get(index+3).contains("67 90");
    }

    private boolean isStartMarker(List<String> lines, int index) {
        return index >= 2
                && lines.get(index).contains("67 90")
                && lines.get(index-1).contains("64")
                && lines.get(index-2).contains("bb 6f 00 00 00");
    }


}
