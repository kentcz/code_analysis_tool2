
package com.kentcz.cat.services; import com.kentcz.cat.configurations.IacaConfiguration;

public class IacaService {

    private String binPath;
    private String libPath;
    private CommandlineService commandlineService;

    public IacaService(IacaConfiguration iacaConfiguration, CommandlineService commandlineService) {
        this.libPath = iacaConfiguration.getLibPath();
        this.binPath = iacaConfiguration.getBinPath();
        this.commandlineService = commandlineService;
    }

    public void latencyAnalysis(String inputFile, String outputFile) {
        String cmd = "LD_LIBRARY_PATH=" + libPath + " " + binPath + " -64 -o " + outputFile + " " + inputFile;
        commandlineService.execute(cmd);
    }
}
