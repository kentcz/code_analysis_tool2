package com.kentcz.cat.services;

import com.kentcz.cat.CatApplication;
import com.kentcz.cat.configurations.*;
import com.kentcz.cat.models.Instruction;
import com.kentcz.cat.resources.Analyzer;
import com.kentcz.cat.resources.HotspotParser;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;
import javax.ws.rs.ext.Provider;

import static com.google.common.base.Preconditions.checkNotNull;

@Module
public class CatModule {
    private final CatConfiguration catConfiguration;
    private final IacaConfiguration iacaConfiguration;
    private final IccConfiguration iccConfiguration;
    private final ObjdumpConfiguration objdumpConfiguration;
    private final OutputConfiguration outputConfiguration;
    private final XedConfiguration xedConfiguration;
    private final String dbPath;
    private final String outputRoot;

    public CatModule(String dbPath, String outputRoot,
                     CatConfiguration catConfiguration,
                     IacaConfiguration iacaConfiguration,
                     IccConfiguration iccConfiguration,
                     ObjdumpConfiguration objdumpConfiguration,
                     OutputConfiguration outputConfiguration,
                     XedConfiguration xedConfiguration) {
        this.dbPath = dbPath;
        this.outputRoot = outputRoot;
        this.catConfiguration = checkNotNull(catConfiguration);
        this.iacaConfiguration = checkNotNull(iacaConfiguration);
        this.iccConfiguration = checkNotNull(iccConfiguration);
        this.objdumpConfiguration = checkNotNull(objdumpConfiguration);
        this.outputConfiguration = checkNotNull(outputConfiguration);
        this.xedConfiguration = checkNotNull(xedConfiguration);
    }

    @Singleton
    @Provides
    VTuneParserService provideVTuneParserService() {
        return new VTuneParserService(dbPath);
    }

    @Singleton
    @Provides
    BasicBlockAnalysis provideBasicBlockAnalysis(VTuneParserService vTuneParserService) {
        return new BasicBlockAnalysis(outputConfiguration.getBasicblockPath(outputRoot), vTuneParserService);
    }

    @Singleton
    @Provides
    CompilerService provideCompilerService(CommandlineService commandlineService) {
        return new IccCompilerService(this.iccConfiguration, commandlineService);
    }

    @Singleton
    @Provides
    IacaService provideIacaService(CommandlineService commandlineService) {
        return new IacaService(this.iacaConfiguration, commandlineService);
    }

    @Singleton
    @Provides
    OutputService providesOutputService() {
        return new OutputService(this.outputConfiguration);
    }

    @Singleton
    @Provides
    ObjdumpService providesObjectdumpService(CommandlineService commandlineService) {
        return new ObjdumpService(objdumpConfiguration, commandlineService);
    }

    @Singleton
    @Provides
    InstructionService providesInstructionService(XedService xedService) {
        return new InstructionService(xedService);
    }

    @Singleton
    @Provides
    BasicBlockService provicesBasicBlockService(InstructionService instructionService,
                                                RegisterService registerService) {
        return new BasicBlockService(CatApplication.class, instructionService, registerService);
    }

    @Singleton
    @Provides
    XedService providesXedService(CommandlineService commandlineService) {
        return new XedService(commandlineService, xedConfiguration);
    }

    @Singleton
    @Provides
    RegisterService providesRegisterService() {
        return new RegisterService();
    }

    @Singleton
    @Provides
    LoopService providesLoopService(VTuneParserService vTuneParserService,
                                    ObjdumpService objdumpService, HotspotParser hotspotParser) {
        return new LoopService(outputRoot, outputConfiguration, vTuneParserService, objdumpService, hotspotParser);
    }

    @Singleton
    @Provides
    KernelSourceParser providesKernelSourceParser() {
        return new KernelSourceParser();
    }

    @Singleton
    @Provides
    HotspotParser providesHotspotParser() {
        return new HotspotParser();
    }

    @Singleton
    @Provides
    Analyzer providesAnalyzer(OutputService outputService,
                              CompilerService compilerService,
                              IacaService iacaService,
                              InstructionService instructionService,
                              BasicBlockAnalysis basicBlockAnalysis,
                              BasicBlockService basicBlockService,
                              VTuneParserService vTuneParserService,
                              ObjdumpService objdumpService,
                              RegisterService registerService,
                              KernelSourceParser kernelSourceParser) {
       return new Analyzer(outputRoot,
               basicBlockAnalysis,
               basicBlockService,
               compilerService,
               iacaService,
               instructionService,
               vTuneParserService,
               objdumpService,
               registerService,
               kernelSourceParser,
               outputConfiguration);
    }

    @Singleton
    @Provides
    CommandlineService providesCommandlineService() {
        return new CommandlineService();
    }
}
