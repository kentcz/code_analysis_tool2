package com.kentcz.cat.services;

import com.google.common.collect.Lists;
import com.kentcz.cat.models.Instruction;
import com.kentcz.cat.models.MemoryOperand;
import com.kentcz.cat.models.Operand;
import com.kentcz.cat.models.VariableOperand;
import com.kentcz.cat.models.objdump.ObjdumpInstruction;
import com.kentcz.cat.models.xed.XedInstruction;

import java.util.List;
import java.util.Random;

public class InstructionService {

    private final XedService xedService;

    public InstructionService(XedService xedService) {
        this.xedService = xedService;
    }

    public String sanitize(String rawAsm) {
        String[] words = rawAsm.split("\\s+");
        String asm = "";
        for (int i = 0; i < words.length; i++) {
            char first_char = words[i].charAt(0);
            if (first_char == '#') { return asm; }
            if (first_char != '<') {
                if (i > 0) {
                    asm += " ";
                }

                // Convert movabs -> mov
                if (words[i].equals("movabs")) {
                    asm += "mov";
                } else {
                    asm += words[i];
                }
            }
        }
        return asm;
    }

    public void validateInstruction(ObjdumpInstruction instruction) {
        XedInstruction xedInstruction = xedService.decode(instruction.getBytes());
        Instruction xedIns = xedInstruction.toInstruction();

        String xedAsm = xedInstruction.toInstruction().toAssembly();
        String objdumpAsm = sanitize(instruction.getAsm());
        if (xedAsm.equals(objdumpAsm)) {
            System.out.println(xedAsm);
        } else {
            System.out.println("Failed: " + objdumpAsm + "!=" + xedAsm );
        }
    }

    public boolean hasRipBase(ObjdumpInstruction instruction) {
        XedInstruction xedInstruction = xedService.decode(instruction.getBytes());
        Instruction xedIns = xedInstruction.toInstruction();
        return xedIns.hasRipBase();
    }

    public ObjdumpInstruction replaceRip(ObjdumpInstruction instruction, String replacementVariable) {
        XedInstruction xedInstruction = xedService.decode(instruction.getBytes());
        Instruction xedIns = xedInstruction.toInstruction();
        Instruction newInstruction = replaceRip(xedIns, replacementVariable);
        return new ObjdumpInstruction(instruction.getAddress(), instruction.getBytes(), newInstruction.toAssembly());
    }

    public Instruction replaceRip(Instruction instruction, String replacementVariable) {
        return instruction.replaceRip(replacementVariable);
    }

    public Instruction decodeInstruction(String bytes) {
        XedInstruction xedInstruction = xedService.decode(bytes);
        return xedInstruction.toInstruction();
    }

    public List<String> readRegisters(ObjdumpInstruction instruction) {
        XedInstruction xedInstruction = xedService.decode(instruction.getBytes());
        return xedInstruction.readRegisters();
    }

    public List<String> writeRegisters(ObjdumpInstruction instruction) {
        XedInstruction xedInstruction = xedService.decode(instruction.getBytes());
        return xedInstruction.writeRegisters();
    }

    public List<String> baseRegisters(Instruction instruction) {
        List<String> registers = Lists.newArrayList();
        for (Operand operand : instruction.getOperands()) {
            if (operand instanceof MemoryOperand) {
                MemoryOperand memoryOperand  = (MemoryOperand) operand;
                if (memoryOperand.hasBase()) {
                    registers.add(memoryOperand.getBase());
                }
            }
        }
        return registers;
    }

    public List<String> indexRegisters(Instruction instruction) {
        List<String> registers = Lists.newArrayList();
        for (Operand operand : instruction.getOperands()) {
            if (operand instanceof MemoryOperand) {
                MemoryOperand memoryOperand  = (MemoryOperand) operand;
                if (memoryOperand.hasIndex()) {
                    registers.add(memoryOperand.getIndex());
                }
            }
        }
        return registers;
    }


    public boolean scrambleable(Instruction instruction) {
        for (Operand operand : instruction.getOperands()) {
            if (operand.isSIMD()) {
                return true;
            }
        }
        return false;
    }

    public Instruction scramble(Instruction instruction) {
        List<Operand> newOperands = Lists.newArrayList();
        Random random = new Random();
        for (Operand operand : instruction.getOperands()) {
            if (operand.isSIMD()) {
                String registerNum = Integer.toString(random.nextInt(16));
                String newRegister = operand.getDetails().substring(0,3) + registerNum;
                newOperands.add(operand.replaceSIMDRegister(newRegister));
            } else {
                newOperands.add(operand);
            }
        }
        return instruction.replaceOperands(newOperands);
    }

}
