package com.kentcz.cat.services;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.kentcz.cat.models.ExportableBasicBlock;
import com.kentcz.cat.models.Instruction;
import com.kentcz.cat.models.objdump.ObjdumpInstruction;

import javax.inject.Inject;
import java.io.*;
import java.util.*;

public class BasicBlockService {

    private final Class application;
    private final InstructionService instructionService;
    private final RegisterService registerService;

    @Inject
    public BasicBlockService(Class application,
                             InstructionService instructionService,
                             RegisterService registerService) {
        this.application = application;
        this.instructionService = instructionService;
        this.registerService = registerService;
    }

    public void exportInstructions(String outputFile, List<ObjdumpInstruction> instructions) {
        System.out.println("exporting " + instructions.size() + " instructions");
        try {
            PrintStream out = new PrintStream(new File(outputFile));
            for (ObjdumpInstruction instruction : instructions) {
                out.println(instruction.getAsm());
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exportCCode(String outputFile, List<ObjdumpInstruction> instructions) {
        exportCCode(outputFile, instructions, false);
    }

    public void exportCCode(String outputFile, List<ObjdumpInstruction> instructions, boolean useIacaTags) {
        try {
            // Template input stream
            InputStream inputStream = application.getResourceAsStream("/templates/cCode.mustache");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            // File output stream
            PrintStream out = new PrintStream(new File(outputFile));
            Writer writer = new OutputStreamWriter(out);

            // Remove RIP references
            List<ObjdumpInstruction> newInstructions = Lists.newArrayList();
            List<String> variables = Lists.newArrayList();
            for (ObjdumpInstruction instruction : instructions) {
                if (instructionService.hasRipBase(instruction)) {
                    String variable = String.format("arr%s", variables.size());
                    variables.add(variable);
                    newInstructions.add(instructionService.replaceRip(instruction, variable));
                } else {
                    newInstructions.add(instruction);
                }
            }

            // Set mustache variables
            HashMap<String, Object> scopes = new HashMap<String, Object>();
            scopes.put("instructions", sanitize(newInstructions));
            scopes.put("variables", variables);
            if (useIacaTags) {
                scopes.put("useIaca", new Boolean(useIacaTags));
            }

            MustacheFactory mf = new DefaultMustacheFactory();
            Mustache mustache = mf.compile(bufferedReader, "example");
            mustache.execute(writer, scopes);
            writer.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<ObjdumpInstruction> sanitize(List<ObjdumpInstruction> instructions) {
        List<ObjdumpInstruction> newInstructions = Lists.newArrayList();
        for (ObjdumpInstruction instruction : instructions) {
            newInstructions.add(new ObjdumpInstruction(instruction.getAddress(),
                    instruction.getBytes(),
                    instructionService.sanitize(instruction.getAsm())));
        }
        return newInstructions;
    }

    public void validateInstructions(List<ObjdumpInstruction> instructions) {
        for (ObjdumpInstruction instruction : instructions) {
            instructionService.validateInstruction(instruction);
        }
    }

    public HashMap<String, Boolean> readRegisters(List<ObjdumpInstruction> instructions) {
        HashMap<String, Boolean> registerSet = registerService.registerSet();
        for (ObjdumpInstruction instruction : instructions) {
            List<String> registers = instructionService.readRegisters(instruction);
            for (String register : registers) {
                String fullRegister = registerService.fullRegister(register);
                if (registerSet.containsKey(fullRegister)) {
                    registerSet.put(fullRegister, true);
                }
            }
        }
        return registerSet;
    }

    public HashMap<String, Boolean> writeRegisters(List<ObjdumpInstruction> instructions) {
        HashMap<String, Boolean> registerSet = registerService.registerSet();
        for (ObjdumpInstruction instruction : instructions) {
            List<String> registers = instructionService.writeRegisters(instruction);
            for (String register : registers) {
                String fullRegister = registerService.fullRegister(register);
                if (registerSet.containsKey(fullRegister)) {
                    registerSet.put(fullRegister, true);
                }
            }
        }
        return registerSet;
    }

    public List<String> unknownRegisters(List<ObjdumpInstruction> instructions) {
        HashMap<String, Boolean> registerSet = registerService.registerSet();
        List<String> unknownRegisters = Lists.newArrayList();
        for (ObjdumpInstruction instruction : instructions) {
            List<String> registers = instructionService.readRegisters(instruction);
            for (String register : registers) {
                String fullRegister = registerService.fullRegister(register);
                if (!registerSet.containsKey(fullRegister)) {
                    unknownRegisters.add(fullRegister);
                }
            }
            registers = instructionService.writeRegisters(instruction);
            for (String register : registers) {
                String fullRegister = registerService.fullRegister(register);
                if (!registerSet.containsKey(fullRegister)) {
                    unknownRegisters.add(fullRegister);
                }
            }
        }
        return unknownRegisters;
    }

    public Map<String, Boolean> getReadRegisters(List<Instruction> instructions) {
        Map<String, Boolean> registerSet = registerService.registerSet();
        for (Instruction instruction : instructions) {
            List<String> registers = instruction.readRegisters();
            for (String register : registers) {
                String fullRegister = registerService.fullRegister(register);
                if (registerSet.containsKey(fullRegister)) {
                    registerSet.put(fullRegister, true);
                }
            }
        }
        return registerSet;
    }

    public Map<String, Boolean> getWriteRegisters(List<Instruction> instructions) {
        Map<String, Boolean> registerSet = registerService.registerSet();
        for (Instruction instruction : instructions) {
            List<String> registers = instruction.writeRegisters();
            for (String register : registers) {
                String fullRegister = registerService.fullRegister(register);
                if (registerSet.containsKey(fullRegister)) {
                    registerSet.put(fullRegister, true);
                }
            }
        }
        return registerSet;
    }

    /**
    public List<String> readRegisters(List<Instruction> instructions) {

        Set<String> unusedRegisters = registerService.generalPurposeRegisters();
        for (Instruction instruction : instructions) {
            List<String> readRegisters = instruction.readRegisters();
            for (String register : readRegisters) {
                unusedRegisters.remove(registerService.fullRegister(register));
            }
            List<String> writeRegisters = instruction.writeRegisters();
            for (String register : writeRegisters) {
                unusedRegisters.remove(registerService.fullRegister(register));
            }
        }

        List<String> outputArray = Lists.newArrayList();
        for (String register : unusedRegisters) {
            outputArray.add(register);
        }
        return outputArray;
    }
     **/

    public List<String> findUnusedRegisters(List<Instruction> instructions) {

        Set<String> unusedRegisters = registerService.generalPurposeRegisters();
        for (Instruction instruction : instructions) {
            List<String> readRegisters = instruction.readRegisters();
            for (String register : readRegisters) {
                unusedRegisters.remove(registerService.fullRegister(register));
            }
            List<String> writeRegisters = instruction.writeRegisters();
            for (String register : writeRegisters) {
                unusedRegisters.remove(registerService.fullRegister(register));
            }
        }

        List<String> outputArray = Lists.newArrayList();
        for (String register : unusedRegisters) {
            outputArray.add(register);
        }
        return outputArray;
    }

    public List<Instruction> decodeInstructions(List<ObjdumpInstruction> instructions) {
        List<Instruction> newInstructions = Lists.newArrayList();
        for (ObjdumpInstruction instruction : instructions) {
            newInstructions.add(instructionService.decodeInstruction(instruction.getBytes()));
        }
        return newInstructions;
    }


    public List<Instruction> unroll(List<Instruction> instructions, int targetInstructions) {
        int numInstruction = instructions.size()-2;
        long unrolls = (long) Math.ceil((double) targetInstructions / (double) numInstruction);

        List<Instruction> unrolledInstructions = Lists.newArrayList();
        List<Instruction> coreInstructions = instructions.subList(0,instructions.size() - 2);
        for (int i = 0; i < unrolls; i++) {
            unrolledInstructions.addAll(coreInstructions);
        }
        unrolledInstructions.add(instructions.get(instructions.size()-2));
        unrolledInstructions.add(instructions.get(instructions.size()-1));
        return unrolledInstructions;
    }


    public boolean scrambleable(List<Instruction> instructions) {
        for (Instruction instruction : instructions) {
            if (instructionService.scrambleable(instruction)) {
                return true;
            }
        }
        return false;
    }

    public List<Instruction> scramble(List<Instruction> instructions) {
        List<Instruction> scrambledInstructions = Lists.newArrayList();
        for (Instruction instruction : instructions) {
            scrambledInstructions.add(instructionService.scramble(instruction));
        }
        return scrambledInstructions;
    }


    public void exportKernel(String outputFile, List<ObjdumpInstruction> instructions, boolean useIacaTags) {                // Remove RIP references
        List<String> ripVariables = Lists.newArrayList();
        List<String> arrayVariables = Lists.newArrayList();
        List<Instruction> kernelInstructions = Lists.newArrayList();

        for (ObjdumpInstruction instruction : instructions) {

            //System.out.println("instruction.getBytes:" + instruction.getBytes());
            Instruction ins = instructionService.decodeInstruction(instruction.getBytes());

            // Replace rip access
            if (ins.hasRipBase()) {
                String variable = String.format("arr%s", ripVariables.size());
                ripVariables.add(variable);
                ins = instructionService.replaceRip(ins, variable);
            }

            // Remove displacement in memory addresses
            ins = ins.removeDisplacement();

            // Remove branch instructions
            if (!ins.isBranch()) {
                kernelInstructions.add(ins);
            }
        }

        // Find two unused registers
        List<String> availableRegisters = findUnusedRegisters(kernelInstructions);
        System.out.println("availableRegisters=" + Joiner.on(", ").join(availableRegisters));
        String loopCounterRegister = "r16";
        String iterationRegister = "r17";
        if (availableRegisters.size() >= 2) {
            loopCounterRegister = availableRegisters.get(0);
            iterationRegister = availableRegisters.get(1);
        }

        // Find arrays
        Map<String, Boolean> readRegisters = getReadRegisters(kernelInstructions);
        Map<String, Boolean> writeRegisters = getWriteRegisters(kernelInstructions);

        Set<String> arrayRegisterSet = Sets.newHashSet();
        Set<String> immovableRegisterSet = Sets.newHashSet();
        for (Instruction instruction : kernelInstructions) {
            List<String> baseRegisters = instructionService.baseRegisters(instruction);
            for (String reg : baseRegisters) {
                String register = registerService.fullRegister(reg);
                arrayRegisterSet.add(register);
                immovableRegisterSet.add(register);
            }
            List<String> indexRegisters = instructionService.indexRegisters(instruction);
            for (String reg : indexRegisters) {
                String register = registerService.fullRegister(reg);
                immovableRegisterSet.add(register);
            }
        }

        ExportableBasicBlock basicBlock = new ExportableBasicBlock(new ArrayList<>(arrayRegisterSet),
                ripVariables, kernelInstructions, loopCounterRegister, iterationRegister, 32);
        exportBasicBlock(outputFile, basicBlock, useIacaTags);
    }


    public void exportBasicBlock(String outputFile, ExportableBasicBlock basicBlock, boolean useIacaTags) {

        try {
            // Template input stream
            InputStream inputStream = application.getResourceAsStream("/templates/basicBlockTemplate.mustache");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            // File output stream
            PrintStream out = new PrintStream(new File(outputFile));
            Writer writer = new OutputStreamWriter(out);

            List<String> assemblyInstructions = Lists.newArrayList();
            for (Instruction instruction : basicBlock.getInstructions()) {
                assemblyInstructions.add(instruction.toAssembly());
            }

            // Set mustache variables
            HashMap<String, Object> scopes = new HashMap<String, Object>();
            scopes.put("instructions", assemblyInstructions);
            scopes.put("arrayVariables", basicBlock.getArrayVariables());
            scopes.put("ripVariables", basicBlock.getRipVariables());
            scopes.put("arraySize", basicBlock.getArraySize());
            scopes.put("iterationRegister", basicBlock.getIterationRegister());
            scopes.put("loopCountRegister", basicBlock.getLoopCountRegister());
            if (useIacaTags) {
                scopes.put("useIaca", new Boolean(useIacaTags));
            }

            MustacheFactory mf = new DefaultMustacheFactory();
            Mustache mustache = mf.compile(bufferedReader, "example");
            mustache.execute(writer, scopes);
            writer.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
