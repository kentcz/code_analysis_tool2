package com.kentcz.cat.services;

import com.google.common.collect.Sets;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class RegisterService {

    private final HashMap<String, String> registerMap;

    public RegisterService() {
        registerMap = buildRegisterMap();
    }

    public HashMap<String, Boolean> registerSet() {
        HashMap<String, Boolean> set = new HashMap<String, Boolean>();

        // rax, rbx, rcx, rdx
        String[] letters = {"a", "b", "c", "d"};
        for (String l : letters) {
            set.put("r" + l + "x", false);
        }

        set.put("rsi", false);
        set.put("rdi", false);
        set.put("rbp", false);
        set.put("rsp", false);

        // r8 - r15
        for (int i = 8; i <= 15; i++) {
            set.put("r" + i, false);
        }

        for (int i = 0; i <= 15; i++) {
            set.put("ymm" + i, false);
        }

        return set;
    }

    private HashMap<String, String> buildRegisterMap() {

        HashMap<String, String> registerMap = new HashMap<>();

        // rax, rbx, rcx, rdx
        String[] letters = {"a", "b", "c", "d"};
        for (String l : letters) {
            registerMap.put("r" + l + "x", "r" + l + "x");
            registerMap.put("e" + l + "x", "r" + l + "x");
            registerMap.put(l + "x", "r" + l + "x");
            registerMap.put(l + "l", "r" + l + "x");
        }

        // rsi
        registerMap.put("rsi", "rsi");
        registerMap.put("esi", "rsi");
        registerMap.put("si", "rsi");
        registerMap.put("sil", "rsi");

        // rdi
        registerMap.put("rdi", "rdi");
        registerMap.put("edi", "rdi");
        registerMap.put("di", "rdi");
        registerMap.put("dil", "rdi");

        // rbp
        registerMap.put("rbp", "rbp");
        registerMap.put("ebp", "rbp");
        registerMap.put("bp", "rbp");
        registerMap.put("bpl", "rbp");

        // rsp
        registerMap.put("rsp", "rsp");
        registerMap.put("esp", "rsp");
        registerMap.put("sp", "rsp");
        registerMap.put("spl", "rsp");

        // r8 - r15
        for (int i = 8; i <= 15; i++) {
            registerMap.put("r" + i, "r" + i);
            registerMap.put("r" + i + "d", "r" + i);
            registerMap.put("r" + i + "w", "r" + i);
            registerMap.put("r" + i + "b", "r" + i);
        }

        for (int i = 0; i <= 15; i++) {
            registerMap.put("ymm" + i, "ymm" + i);
            registerMap.put("xmm" + i, "ymm" + i);
        }

        return registerMap;
    }

    public String fullRegister(String register) {
        String reg = register.toLowerCase();
        if (registerMap.containsKey(reg)) {
            return registerMap.get(reg);
        }
        return reg;
    }

    public void printRegisterUsage(HashMap<String, Boolean> readRegisters,
                                   HashMap<String, Boolean> writeRegisters,
                                   List<String> unknownRegisters,
                                   String outputFile) {
        try {
            PrintStream out = new PrintStream(new File(outputFile));
            printRegisterUsage(readRegisters, writeRegisters, unknownRegisters, out);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void printRegisterUsage(HashMap<String, Boolean> readRegisters,
                                   HashMap<String, Boolean> writeRegisters,
                                   List<String> unknownRegisters,
                                   PrintStream out) {
        for (String register : readRegisters.keySet()) {
            out.print(register + "\t");
            if (readRegisters.get(register) == true && writeRegisters.get(register) == true) {
                out.print("RW");
            } else if (readRegisters.get(register) == true && writeRegisters.get(register) == false) {
                out.print("R");
            } else if (readRegisters.get(register) == false && writeRegisters.get(register) == true) {
                out.print("W");
            } else {
                out.print("-");
            }
            out.println();
        }

        out.println("\nunknown:");
        for (String reg : unknownRegisters) {
            out.println(reg);
        }
    }

    public Set<String>  generalPurposeRegisters() {
        Set<String> registers = Sets.newHashSet();
        registers.add("rax");
        registers.add("rbx");
        registers.add("rcx");
        registers.add("rdx");
        registers.add("rsi");
        registers.add("rdi");
        registers.add("r8");
        registers.add("r9");
        registers.add("r10");
        registers.add("r11");
        registers.add("r12");
        registers.add("r13");
        registers.add("r14");
        registers.add("r15");
        return registers;
    }
}