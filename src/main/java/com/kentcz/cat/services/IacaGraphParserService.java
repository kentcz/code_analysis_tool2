package com.kentcz.cat.services;

import com.kentcz.cat.models.iaca.DiagraphEdge;
import com.kentcz.cat.models.iaca.IacaInstruction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class IacaGraphParserService {

    private static final Pattern uopPattern = Pattern.compile("(\\d*)([F\\*\\^#@!]*)");

    public static boolean isHeadingLine(String line) {
        return line.startsWith("digraph ");
    }
    public static boolean isVertexLine(String line) {
        return line.contains("[label=");
    }
    public static boolean isEdgeLine(String line) {
        return line.contains("->");
    }

    public static DiagraphEdge parseEdgeLine(String line) {
        String[] nodes = line.split("->");
        int parentId = Integer.parseInt(nodes[0].trim());
        int childId = Integer.parseInt(nodes[1].trim());
        return new DiagraphEdge(parentId, childId);
    }

    public static List<String> readDotFile(String fileName) {
        List<String> lines = new ArrayList<String>();
        try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            for(String line; (line = br.readLine()) != null; ) {
                lines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    public static int getVertexId(String line) {
        String[] words = line.split("\\s+");
        return Integer.parseInt(words[0]);
    }

    public static boolean verifyVertices(List<String> lines, List<IacaInstruction> instructions) {
        for (String line : lines) {
            if (isVertexLine(line)) {
                int vertexId = getVertexId(line);
                if (instructions.size() <= vertexId
                        || instructions.get(vertexId) == null
                        || !line.contains(instructions.get(vertexId-1).getAsm())) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void updateConnection(List<String> lines, List<IacaInstruction> instructions) {
        for (String line : lines) {
            DiagraphEdge edge = parseEdgeLine(line);
            if (edge.getChildId() != edge.getParentId()) {
                IacaInstruction childInstruction = instructions.get(edge.getChildId() - 1);
                IacaInstruction parentInstruction = instructions.get(edge.getParentId() - 1);
                childInstruction.addDependency(parentInstruction);
            }
        }
    }
}
