package com.kentcz.cat.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kentcz.cat.configurations.XedConfiguration;
import com.kentcz.cat.models.xed.XedInstruction;

import javax.inject.Inject;

public class XedService {

    private final CommandlineService commandlineService;
    private final String binPath;

    @Inject
    public XedService(CommandlineService commandlineService, XedConfiguration xedConfiguration) {
        this.commandlineService = commandlineService;
        this.binPath = xedConfiguration.getBinPath();
    }

    public XedInstruction decode(String bytes) {
        String cmd = binPath + " -64 " + bytes;
        String output = commandlineService.execute(cmd, false);

        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(output, XedInstruction.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
