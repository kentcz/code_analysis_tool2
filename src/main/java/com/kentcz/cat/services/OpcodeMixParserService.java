package com.kentcz.cat.services;

import com.google.common.collect.Lists;
import com.kentcz.cat.models.opcodemix.OpcodeSample;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;

public class OpcodeMixParserService {

    public OpcodeMixParserService() { }

    public boolean isDynamicSection(String line) {
        return line.startsWith("# $dynamic-counts");
    }

    public OpcodeSample parseLine(String line) {

        if (line.startsWith("#") || line.trim().length() == 0) { return null; }

        String[] words = line.trim().split("\\s+");
        int id = Integer.parseInt(words[0]);
        String opcode = words[1];
        long count = Long.parseLong(words[2]);
        long predicatedCount = Long.parseLong(words[3]);
        return new OpcodeSample(id, opcode, count, predicatedCount);
    }

    public List<OpcodeSample> parseFile(String filename) {
        List<OpcodeSample> samples = Lists.newArrayList();
        boolean inDynamicSection = false;
        try {
            BufferedReader br = new BufferedReader(new FileReader(filename));
            for (String line; (line = br.readLine()) != null; ) {
                if (isDynamicSection(line)) { inDynamicSection = true; }
                if (inDynamicSection) {
                    OpcodeSample sample = parseLine(line);
                    if (sample != null) {
                        samples.add(sample);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return samples;
    }

    public long total(List<OpcodeSample> samples) {
        for (OpcodeSample sample : samples) {
            if (sample.isTotal()) { return sample.countTotal(); }
        }
        System.out.println("Error: total is not found");
        return -1;
    }

    public double countBranches(List<OpcodeSample> samples) {
        long count = 0;
        for (OpcodeSample sample : samples) {
            if (sample.isBranch()) { count += sample.countTotal(); }
        }
        return count / ((double) total(samples));
    }

    public double countAVX(List<OpcodeSample> samples) {
        long count = 0;
        for (OpcodeSample sample : samples) {
            if (sample.isAVX()) { count += sample.countTotal(); }
        }
        return count / ((double) total(samples));
    }

    public double countAVXfp(List<OpcodeSample> samples) {
        long count = 0;
        for (OpcodeSample sample : samples) {
            if (sample.isAVXfp()) { count += sample.countTotal(); }
        }
        return count / ((double) total(samples));
    }

    public double countVectorized(List<OpcodeSample> samples) {
        long count = 0;
        for (OpcodeSample sample : samples) {
            if (sample.isVectorized()) { count += sample.countTotal(); }
        }
        return count / ((double) total(samples));
    }

    public double countMops(List<OpcodeSample> samples) {
        long count = 0;
        for (OpcodeSample sample : samples) {
            if (sample.isMemoryRead() || sample.isMemoryWrite()) { count += sample.countTotal(); }
        }
        return count / ((double) total(samples));
    }

    public double countReads(List<OpcodeSample> samples) {
        long count = 0;
        for (OpcodeSample sample : samples) {
            if (sample.isMemoryRead()) { count += sample.countTotal(); }
        }
        return count / ((double) total(samples));
    }

    public double countReads16(List<OpcodeSample> samples) {
        long count = 0;
        for (OpcodeSample sample : samples) {
            if (sample.isMemoryRead16() || sample.isMemoryRead32()) { count += sample.countTotal(); }
        }
        return count / ((double) total(samples));
    }

    public double countStack(List<OpcodeSample> samples) {
        long count = 0;
        for (OpcodeSample sample : samples) {
            if (sample.isStack()) { count += sample.countTotal(); }
        }
        return count / ((double) total(samples));
    }
}
