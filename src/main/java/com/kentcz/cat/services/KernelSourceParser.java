package com.kentcz.cat.services;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import com.kentcz.cat.models.Instruction;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

public class KernelSourceParser {
    public KernelSourceParser() {
    }

    public List<String> loadSourceFile(String sourceFile) {
        List<String> lines = Lists.newArrayList();
        try {
            lines = Files.readLines(new File(sourceFile), Charsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }


    public void writeSourceFile(String outputFile, List<String> lines, List<Instruction> instructions) {
        try {
            PrintStream out = new PrintStream(new File(outputFile));
            int lineCount = 0;
            while (lineCount < lines.size() && !isStartBlock(lines, lineCount)) {
                out.println(lines.get(lineCount));
                lineCount++;
            }

            for (Instruction instruction : instructions) {
                out.println(String.format("    %s", instruction.toAssembly()));
            }

            while (lineCount < lines.size() && !isStopBlock(lines, lineCount)) {
                lineCount++;
            }

            while (lineCount < lines.size()) {
                out.println(lines.get(lineCount));
                lineCount++;
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isStartBlock(List<String> lines, int index) {
        return lines.get(index).trim().equals("kernelLoop:");
    }

    private boolean isStopBlock(List<String> lines, int index) {
        return lines.size() > index + 3
                && lines.get(index+2).trim().equals("#ifdef IACA")
                && lines.get(index+3).trim().equals("mov ebx, 222");
    }
}
