package com.kentcz.cat.services;

public interface CompilerService {
    void compile(String inputFile, String outputFile);
    void compile(String inputFile, String outputFile, String flags);
    void compilerVersion();
}
