package com.kentcz.cat.services;

import com.kentcz.cat.configurations.OutputConfiguration;
import com.kentcz.cat.models.BlockLocation;
import com.kentcz.cat.models.hotspots.HotspotNode;
import com.kentcz.cat.resources.HotspotParser;

import java.io.File;
import java.io.PrintStream;
import java.util.List;

public class LoopService {

    private final String outputDirectory;
    private final VTuneParserService vTuneParserService;
    private final ObjdumpService objdumpService;
    private final HotspotParser hotspotParser;

    public LoopService(String outputRoot,
                       OutputConfiguration outputConfiguration,
                       VTuneParserService vTuneParserService,
                       ObjdumpService objdumpService,
                       HotspotParser hotspotParser) {
        this.vTuneParserService = vTuneParserService;
        this.objdumpService = objdumpService;
        this.hotspotParser = hotspotParser;
        this.outputDirectory = outputConfiguration.getLoopPath(outputRoot);
    }

    public void dumpLoopNests(String hotspotFile) {
        try {
            String loopList = String.format("%s/%s", outputDirectory, "loops.txt");
            String loopSummary = String.format("%s/%s", outputDirectory, "loopsSummary.txt");
            PrintStream loopListOut = new PrintStream(new File(loopList));
            PrintStream loopSummaryOut = new PrintStream(new File(loopSummary));
            List<HotspotNode> hotspots = hotspotParser.parseFile(hotspotFile);
            for (HotspotNode node : hotspots) {

                if (node.isLeaf() && node.getTotalTime() > 1.0) {
                    List<BlockLocation> blocks = vTuneParserService.extractLoopNestDetails(node.getLabel());
                    for (BlockLocation block : blocks) {
                        String nodeName = String.format("%s_%s_%s", block.getBinName(),
                                block.getStartAddress(),
                                block.getEndAddress());

                        String moduleDumpFile = String.format("%s/%s", outputDirectory, block.getBinName());
                        objdumpService.dumpBinary(block.getBinPath(), moduleDumpFile);
                        objdumpService.exportLines(moduleDumpFile,
                                String.format("%s/%s.bin", outputDirectory, nodeName),
                                block.getStartAddress(),
                                block.getEndAddress());
                        loopSummaryOut.println(String.format(
                                "%s,%s,%s",
                                Double.toString(node.getTotalTime()),
                                nodeName,
                                node.getLabel()));
                        loopListOut.println(nodeName);
                    }
                }
            }
            loopListOut.close();
            loopSummaryOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
