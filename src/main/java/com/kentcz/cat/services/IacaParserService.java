package com.kentcz.cat.services;

import com.kentcz.cat.models.iaca.IacaBlock;
import com.kentcz.cat.models.iaca.IacaInstruction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IacaParserService {

    private static final Pattern blockThrouputPattern = Pattern.compile("^Block Throughput:\\s([\\d\\.]*)\\sCycles");
    private static final Pattern intsturctionPattern = Pattern.compile("^|\\s(\\d*)([F\\*\\^#@!]*)\\s*|\\s*|");
    private static final Pattern uopPattern = Pattern.compile("(\\d*)([F\\*\\^#@!]*)");

    private static final Pattern portPattern = Pattern.compile("(\\d*)([F\\*\\^#@!]*)");

    public static boolean isBlockThroughputLine(String line) {
        return line.startsWith("Block Throughput:");
    }

    public static double parseBlockThroughputLine(String line) {
        Matcher matcher = blockThrouputPattern.matcher(line);
        matcher.find();
        //System.out.println("matcher.group(0)=" + matcher.group(0));
        //System.out.println("matcher.group(1)=" + matcher.group(1));
        return Double.parseDouble(matcher.group(1));
    }

    public static boolean isTotalUopsLine(String line) {
        return line.startsWith("Total Num Of Uops:");
    }

    public double parseTotalUopsLine(String line) {
        String label = "Total Num Of Uops:";
        return Double.parseDouble(line.substring(label.length()));
    }

    public static boolean isCycleHeadingLine(String line) {
        return line.startsWith("| Cycles |");
    }

    public static boolean isPortHeadingLine(String line) {
        return line.startsWith("|  Port  |");
    }

    public static boolean isInstructionHeadingLine(String line) {
        return line.startsWith("|  Uops  |");
    }

    public static boolean isInstructionLine(String line) {

        if (isCycleHeadingLine(line) || isPortHeadingLine(line) || isInstructionHeadingLine(line)) {
            return false;
        }

        String[] cols = line.split("\\|");
        return cols.length > 11;
    }

    public static IacaInstruction parseInstructionLine(String line) {
        // Default values
        int numUops = 0;
        String asm = "";
        double port0 = 0;
        double port0D = 0;
        double port1 = 0;
        double port2 = 0;
        double port2D = 0;
        double port3 = 0;
        double port3D = 0;
        double port4 = 0;
        double port5 = 0;
        double port6 = 0;
        double port7 = 0;

        String[] cols = line.split("\\|");

        // Parse Num of Uops
        Matcher uopMatcher = uopPattern.matcher(cols[1].trim());
        uopMatcher.find();
        numUops = Integer.parseInt(uopMatcher.group(1));

        // Parse asm
        asm = cols[11].trim();

        // Parse Port0
        port0 = parseDouble(cols[2].substring(0,5));
        port0D = parseDouble(cols[2].substring(6));

        // Parse Port1
        port1 = parseDouble(cols[3]);

        // Parse Port2
        port2 = parseDouble(cols[4].substring(0,5));
        port2D = parseDouble(cols[4].substring(6));

        // Parse Port3
        port3 = parseDouble(cols[5].substring(0,5));
        port3D = parseDouble(cols[5].substring(6));

        // Parse Port4
        port4 = parseDouble(cols[6]);

        // Parse Port5
        port5 = parseDouble(cols[7]);

        // Parse Port6
        port6 = parseDouble(cols[8]);

        // Parse Port7
        port7 = parseDouble(cols[9]);

        return new IacaInstruction(numUops,
                asm,
                port0,
                port0D,
                port1,
                port2,
                port2D,
                port3,
                port3D,
                port4,
                port5,
                port6,
                port7);
    }

    public static IacaBlock parseFile(String fileName) {
        int numUops = 0;
        double throughput = 0;
        List<IacaInstruction> instructionLists = new ArrayList<IacaInstruction>();

        try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            for(String line; (line = br.readLine()) != null; ) {
                if (isBlockThroughputLine(line)) {
                    throughput = parseBlockThroughputLine(line);
                } else if (isInstructionLine(line)) {
                    instructionLists.add(parseInstructionLine(line));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new IacaBlock(throughput, instructionLists);
    }

    private static double parseDouble(String str) {
        if (str.trim().length() == 0) {
            return 0;
        }
        return Double.parseDouble(str.trim());
    }
}
