package com.kentcz.cat.services;

import com.kentcz.cat.configurations.IccConfiguration;

import javax.inject.Inject;

public class IccCompilerService implements CompilerService {

    private final CommandlineService commandlineService;
    private final String binPath;

    @Inject
    public IccCompilerService(IccConfiguration iccConfiguration, CommandlineService commandlineService) {
        this.binPath = iccConfiguration.getBinPath();
        this.commandlineService = commandlineService;
    }

    public void compile(String sourceFile, String outputFile) {
        compile(sourceFile, outputFile, "");
    }

    public void compile(String sourceFile, String outputFile, String flags) {
        String cmd = String.format("%s -fasm-blocks %s -c %s -o %s",
                binPath,
                flags,
                sourceFile,
                outputFile);
        commandlineService.execute(cmd);
    }

    public void compilerVersion() {
        String cmd = binPath + " --version";
        commandlineService.execute(cmd);
    }
}
