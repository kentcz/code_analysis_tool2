package com.kentcz.cat.services;

import com.kentcz.cat.models.vtune.VTuneBasicBlock;

import java.util.List;

public class BasicBlockAnalysis {

    private final VTuneParserService vTuneParserService;
    private final String outputPath;

    public BasicBlockAnalysis(String outputPath, VTuneParserService vTuneParserService) {
        this.outputPath = outputPath;
        this.vTuneParserService = vTuneParserService;
    }

    public void exportBasicBlockDetails() {
        vTuneParserService.exportBasicBlocks(outputPath);
    }
}
