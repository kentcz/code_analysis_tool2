package com.kentcz.cat.services;

import com.kentcz.cat.configurations.OutputConfiguration;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class OutputService {

    private final PrintStream outputstream;

    @Inject
    public OutputService(OutputConfiguration outputConfiguration) {
        PrintStream stream = null;
        try {
            stream = new PrintStream(new File(outputConfiguration.getOutputPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.outputstream = stream;
    }

    public PrintStream getOutputstream() {
        return outputstream;
    }
}
