package com.kentcz.cat;

import com.kentcz.cat.configurations.CatConfiguration;
import com.kentcz.cat.services.CatModule;
import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.setup.Bootstrap;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;

public class DumpLoopsCommand extends ConfiguredCommand<CatConfiguration>{
    public DumpLoopsCommand() {
        super("dump-loops-command", "Dump Loop Nests Command");
    }


    @Override
    public void configure(Subparser subparser) {
        super.configure(subparser);
        subparser.addArgument("-o", "--output")
                .action(Arguments.store())
                .help("Root of the output directory");
        subparser.addArgument("-i", "--input")
                .action(Arguments.store())
                .help("vtune mysqlite database file");
        subparser.addArgument("-f", "--hotspots")
                .action(Arguments.store())
                .help("VTune hotspots_td.txt report file");
    }


    @Override
    protected void run(Bootstrap<CatConfiguration> bootstrap,
                       Namespace namespace,
                       CatConfiguration catConfiguration) throws Exception {

        System.out.println("Stating the Import Kernel Command");
        String outputPath = namespace.getString("output");
        String hotspotFile = namespace.getString("hotspots");
        String dbFile = namespace.getString("input");
        System.out.println("outputPath=" + outputPath);
        System.out.println("dbFile=" + dbFile);
        System.out.println("hotspotFile=" + hotspotFile);

        CatComponent catComponent = DaggerCatComponent.builder()
                .catModule(new CatModule(dbFile, outputPath, catConfiguration,
                        catConfiguration.getIacaConfiguration(),
                        catConfiguration.getIccConfiguration(),
                        catConfiguration.getObjdumpConfiguration(),
                        catConfiguration.getOutputConfiguration(),
                        catConfiguration.getXedConfiguration()))
                .build();
        catComponent.getLoopService().dumpLoopNests(hotspotFile);
    }
}
