package com.kentcz.cat;

import com.kentcz.cat.configurations.CatConfiguration;
import com.kentcz.cat.models.hotspots.HotspotNode;
import com.kentcz.cat.resources.HotspotParser;
import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.setup.Bootstrap;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;

import java.io.File;
import java.io.PrintStream;
import java.util.List;

public class HotspotCommand extends ConfiguredCommand<CatConfiguration> {

    public HotspotCommand() {
        super("hotspot-command", "Analyze hotspot report");
    }

    @Override
    public void configure(Subparser subparser) {
        super.configure(subparser);
        subparser.addArgument("-o", "--output")
                .action(Arguments.store())
                .help("Root of the output directory");
        subparser.addArgument("-i", "--input")
                .action(Arguments.store())
                .help("Path to the sqlite database file");
    }

    @Override
    protected void run(Bootstrap<CatConfiguration> bootstrap,
                       Namespace namespace,
                       CatConfiguration catConfiguration) throws Exception {

        System.out.println("Stating the Hotspot command");
        String outfile = namespace.getString("output");
        String inputFile = namespace.getString("input");

        HotspotParser hotspotParser = new HotspotParser();
        List<HotspotNode> nodes = hotspotParser.parseFile(inputFile);

        try {
            PrintStream out = new PrintStream(new File(outfile));
            hotspotParser.printLoopNests(nodes, 1.0, out);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Count=" + hotspotParser.loopNestCount(nodes, 0.0));
        System.out.println("Time=" + hotspotParser.loopNestTime(nodes, 0.0));
        System.out.println("Count=" + hotspotParser.loopNestCount(nodes, 1.0));
        System.out.println("Time=" + hotspotParser.loopNestTime(nodes, 1.0));

    }
}
