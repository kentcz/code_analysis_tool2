package com.kentcz.cat.resources;

import com.google.common.collect.Lists;
import com.kentcz.cat.models.hotspots.HotspotNode;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HotspotParser {

    @Inject
    public HotspotParser() { }

    private static final Pattern hotspotPattern = Pattern.compile("^(\\s+)\\[(.*)\\]\\s+(\\S+)\\s+([\\d\\.]*)%\\s*([\\d\\.]*)\\s*$");

    public HotspotNode parseLine(String line) {
        try {
            Matcher hotspotMatcher = hotspotPattern.matcher(line);
            if (hotspotMatcher.find() && hotspotMatcher.groupCount() == 5) {
                int level = hotspotMatcher.group(1).length();
                String label = String.format("[%s]", hotspotMatcher.group(2));
                String module = String.format("%s", hotspotMatcher.group(3));
                double totalTime = Double.parseDouble(hotspotMatcher.group(4));
                double selfTime = Double.parseDouble(hotspotMatcher.group(5));

                HotspotNode node = new HotspotNode(totalTime, selfTime, label, module, level);
                //System.out.println(node);
                return node;
            }
        } catch (Exception e) {

        }
        return null;
    }


    public List<HotspotNode> parseFile(String filename) {

        List<HotspotNode> nodes = Lists.newArrayList();
        try {
            // Parse all hotspot nodes
            BufferedReader br = new BufferedReader(new FileReader(filename));
            for (String line; (line = br.readLine()) != null; ) {
                HotspotNode node = parseLine(line);
                if (node != null) {
                    nodes.add(node);
                }
            }

            for (int i = 0; i < nodes.size(); i++) {
                HotspotNode node = nodes.get(i);
                for (int j = i + 1; j < nodes.size() && nodes.get(j).getLevel() > node.getLevel(); j++) {
                    if (nodes.get(j).getLevel() == node.getLevel() + 1) {
                        node.addChild(nodes.get(j));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Done parsing");
        return nodes;
    }

    public double loopNestTime(List<HotspotNode> nodes, double threshold) {
        double total = 0;
        for (HotspotNode node : nodes) {
            if (node.getChildren().size() == 0 && node.getTotalTime() > threshold) {
                total += node.getTotalTime();
            }
        }
        return total;
    }

    public int loopNestCount(List<HotspotNode> nodes, double threshold) {
        int count = 0;
        for (HotspotNode node : nodes) {
            if (node.getChildren().size() == 0 && node.getTotalTime() > threshold) {
                count++;
            }
        }
        return count;
    }

    public void printLoopNests(List<HotspotNode> nodes, double threshold, PrintStream output) {
        for (HotspotNode node : nodes) {
            if (node.getChildren().size() == 0) {
                System.out.println(node);
            }
        }

        // Sort list
        Collections.sort(nodes, new Comparator<HotspotNode>() {
            @Override
            public int compare(HotspotNode a, HotspotNode b) {
                Double aDouble = a.getTotalTime();
                Double bDouble = b.getTotalTime();
                return bDouble.compareTo(aDouble);
            }
        });

        for (HotspotNode node : nodes) {
            if (node.getChildren().size() == 0 && node.getTotalTime() > threshold) {
                output.println(node);
            }
        }
    }
}
