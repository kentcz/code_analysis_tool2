package com.kentcz.cat.resources;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.google.common.collect.Lists;
import com.kentcz.cat.CatApplication;
import com.kentcz.cat.configurations.OutputConfiguration;
import com.kentcz.cat.dao.DaoFactory;
import com.kentcz.cat.dao.InstructionDao;
import com.kentcz.cat.models.Instruction;
import com.kentcz.cat.models.iaca.IacaBlock;
import com.kentcz.cat.models.iaca.IacaInstruction;
import com.kentcz.cat.models.objdump.ObjdumpInstruction;
import com.kentcz.cat.models.vtune.VTuneBasicBlock;
import com.kentcz.cat.services.*;

import javax.inject.Inject;
import java.io.*;
import java.util.HashMap;
import java.util.List;

public class Analyzer {

    private final String outputRoot;
    private final BasicBlockAnalysis basicBlockAnalysis;
    private final BasicBlockService basicBlockService;
    private final CompilerService compilerService;
    private final IacaService iacaService;
    private final InstructionService instructionService;
    private final VTuneParserService vTuneParserService;
    private final ObjdumpService objdumpService;
    private final RegisterService registerService;
    private final KernelSourceParser kernelSourceParser;
    private final OutputConfiguration outputConfiguration;

    @Inject
    public Analyzer(String outputRoot,
                    BasicBlockAnalysis basicBlockAnalysis,
                    BasicBlockService basicBlockService,
                    CompilerService compilerService,
                    IacaService iacaService,
                    InstructionService instructionService,
                    VTuneParserService vTuneParserService,
                    ObjdumpService objdumpService,
                    RegisterService registerService,
                    KernelSourceParser kernelSourceParser,
                    OutputConfiguration outputConfiguration) {
        this.basicBlockAnalysis = basicBlockAnalysis;
        this.basicBlockService = basicBlockService;
        this.compilerService = compilerService;
        this.iacaService = iacaService;
        this.instructionService = instructionService;
        this.vTuneParserService = vTuneParserService;
        this.objdumpService = objdumpService;
        this.outputConfiguration = outputConfiguration;
        this.registerService = registerService;
        this.kernelSourceParser = kernelSourceParser;
        this.outputRoot = outputRoot;
    }


    public void analyzeBasicBlock(String dumpFile, String shortName) {
        String baseName = String.format("%s/%s",
                outputConfiguration.getBasicblockPath(outputRoot),
                shortName);
        analyzeBasicBlock(baseName, objdumpService.extractBlockFromDump(dumpFile));
    }

    public void analyzeBasicBlock(String baseName, List<ObjdumpInstruction> instructions) {
        System.out.println("runAnalysis()");

        // Generate C Code
        basicBlockService.exportKernel(baseName + ".c", instructions, false);

        // Compile C code
        compilerService.compile(baseName + ".c", baseName + ".o");
        compilerService.compile(baseName + ".c", baseName + "_iaca.o", "-DIACA");

        // Generate IACA report
        iacaService.latencyAnalysis(baseName + "_iaca.o", baseName + "_iaca.iaca_latency");

        //basicBlockService.validateInstructions(instructions);
    }


    public void analyzeBasicBlocks() {
        vTuneParserService.exportBasicBlocks(outputConfiguration.getDumpPath(outputRoot));
        List<VTuneBasicBlock> blocks = vTuneParserService.getBasicBlocks();

        List<String> kernelList = Lists.newArrayList();
        for (VTuneBasicBlock block : blocks) {
            String shortName = String.format("%s_%s_%s",
                    block.getBinName(),
                    block.getDisplayAddress(),
                    block.getDisplayLastAddress());
            String baseName = String.format("%s/%s_%s_%s",
                    outputConfiguration.getBasicblockPath(outputRoot),
                    block.getBinName(),
                    block.getDisplayAddress(),
                    block.getDisplayLastAddress());
            String dumpFile = String.format("%s/%s", outputConfiguration.getDumpPath(outputRoot), block.getBinName());
            List<ObjdumpInstruction> instructions = objdumpService.extractBlockFromBinary(block.getBinPath(),
                    dumpFile,
                    block.getDisplayAddress(),
                    block.getDisplayLastAddress());
            analyzeBasicBlock(baseName, instructions);
            kernelList.add(shortName);
        }

        // Print list of kernels to file
        try {
            String kernelListFile = String.format("%s/kernelListFile.log",
                    outputConfiguration.getBasicblockPath(outputRoot));
            PrintStream out = new PrintStream(new File(kernelListFile));
            for (String kernel : kernelList) { out.println(kernel); };
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void scrambleSourceFile(String sourceFile, String shortName) {
        String baseName = String.format("%s/%s",
                outputConfiguration.getDumpPath(outputRoot),
                shortName);
        compilerService.compile(sourceFile, baseName + "_iaca2.o", "-DIACA");
        objdumpService.dumpBinary(baseName + "_iaca2.o", baseName + "_iaca2.bin");
        List<ObjdumpInstruction> objdumpInstructions = objdumpService.extractBlockFromIacaDump(baseName + "_iaca2.bin");
        System.out.println("decoding " + objdumpInstructions.size() + " instructions");
        List<Instruction> instructions = basicBlockService.decodeInstructions(objdumpInstructions);
        List<String> lines = kernelSourceParser.loadSourceFile(sourceFile);
        System.out.println("writing " + lines.size() + " lines");

        List<Instruction> outputInstructions = instructions.subList(0,instructions.size()-1);
        kernelSourceParser.writeSourceFile(baseName + "_updated.c", lines, outputInstructions);

        // scramble instructions
        if (basicBlockService.scrambleable(instructions)) {

            String scramblePath = String.format("%s/%s/",
                    outputConfiguration.getBasicblockPath(outputRoot),
                    shortName);
            outputConfiguration.createDir(scramblePath);

            for (int i = 0; i < 10; i++) {
                String scrambledBaseName =  String.format("%s%s_s%s",
                        scramblePath,
                        shortName,
                        Integer.toString(i));
                System.out.println("Scrambling " + scrambledBaseName);
                List<Instruction> scrambledInstructions = basicBlockService.scramble(instructions);

                // Remove last (branch) instruction
                List<Instruction> outputInstruction = scrambledInstructions.subList(0,scrambledInstructions.size()-1);

                kernelSourceParser.writeSourceFile(scrambledBaseName + ".c", lines, outputInstruction);
            }
        }

    }

    public void loadSourceFile(String sourceFile, String shortName) {
        String baseName = String.format("%s/%s",
                outputConfiguration.getDumpPath(outputRoot),
                shortName);
        compilerService.compile(sourceFile, baseName + "_iaca2.o", "-DIACA");
        objdumpService.dumpBinary(baseName + "_iaca2.o", baseName + "_iaca2.bin");
        List<ObjdumpInstruction> objdumpInstructions = objdumpService.extractBlockFromIacaDump(baseName + "_iaca2.bin");
        System.out.println("decoding " + objdumpInstructions.size() + " instructions");
        List<Instruction> instructions = basicBlockService.decodeInstructions(objdumpInstructions);
        List<String> lines = kernelSourceParser.loadSourceFile(sourceFile);
        System.out.println("writing" + lines.size() + " lines");
        kernelSourceParser.writeSourceFile(baseName + "_updated.c", lines, instructions);
    }

    public void runAnalysis() {
        System.out.println("runAnalysis()");
        vTuneParserService.exportBasicBlocks(outputConfiguration.getDumpPath(outputRoot));
        List<VTuneBasicBlock> blocks = vTuneParserService.getBasicBlocks();
        for (VTuneBasicBlock block : blocks) {
             String baseName = String.format("%s/%s_%s_%s",
                    outputConfiguration.getDumpPath(outputRoot),
                    block.getBinName(),
                    block.getDisplayAddress(),
                    block.getDisplayLastAddress());

            System.out.println(String.format("%s/%s:%s:%s",
                    block.getBinPath(),
                    block.getBinName(),
                    block.getStartAddress(),
                    block.getLastInsAddress()));
            String binaryFile = String.format("%s/%s", block.getBinPath(), block.getBinName());
            String dumpFile = String.format("%s/%s", outputConfiguration.getDumpPath(outputRoot), block.getBinName());
            objdumpService.exportLines(dumpFile,
                    baseName + ".bin",
                    block.getDisplayAddress(),
                    block.getDisplayLastAddress());
            List<ObjdumpInstruction> instructions = objdumpService.extractBlockFromBinary(block.getBinPath(),
                    dumpFile,
                    block.getDisplayAddress(),
                    block.getDisplayLastAddress());

            // Export block
            basicBlockService.exportInstructions(baseName + ".asm", instructions);

            // Generate C Code
            basicBlockService.exportCCode(baseName + ".c", instructions);
            basicBlockService.exportKernel(baseName + "_full.c", instructions, false);
            basicBlockService.exportCCode(baseName + "_iaca.c", instructions, true);

            // Compile C code
            compilerService.compile(baseName + ".c", baseName + ".o");
            compilerService.compile(baseName + "_iaca.c", baseName + "_iaca.o");
            compilerService.compile(baseName + "_full.c", baseName + "_full.o");

            // Generate IACA report
            iacaService.latencyAnalysis(baseName + "_iaca.o", baseName + "_iaca.iaca_latency");

            //basicBlockService.validateInstructions(instructions);

            // Evaluate register usage
            HashMap<String, Boolean> readRegisters = basicBlockService.readRegisters(instructions);
            HashMap<String, Boolean> writeRegisters = basicBlockService.writeRegisters(instructions);
            List<String> unknownRegisters = basicBlockService.unknownRegisters(instructions);
            registerService.printRegisterUsage(readRegisters, writeRegisters, unknownRegisters, baseName + ".reg");
        }
    }


    public void runIacaAnalysis() {
        System.out.println("runAnalysis()");

        basicBlockAnalysis.exportBasicBlockDetails();

        compilerService.compile("this", "that");
        iacaService.latencyAnalysis("/home/kentcz/scratch/llcode/test.o", "/home/kentcz/scratch/llcode/test.iaca");
        String line = "Block Throughput: 8.90 Cycles       Throughput Bottleneck: FrontEnd";
        System.out.println("isBlockThroughputLine()=" + IacaParserService.isBlockThroughputLine(line));
        double throughput = IacaParserService.parseBlockThroughputLine(line);
        System.out.println("throughput=" + throughput);
        IacaBlock block = IacaParserService.parseFile("/home/kentcz/scratch/llcode/test.iaca");
        System.out.println("block=" + block.toString());

        for (IacaInstruction instruction : block.getInstructions()) {
            System.out.println(instruction.toString());
        }


        //DaoFactory daoFactory = new DaoFactory();
        //InstructionDao instructionDao = daoFactory.buildInstructionDao("jdbc:sqlite:/home/kentcz/research/proxy_apps/output/XSBench/top/sqlite-db/dicer.db");
        //instructionDao.findById(1);

    }
}
