package com.kentcz.cat;

import com.kentcz.cat.dao.JDBIModule;
import com.kentcz.cat.resources.Analyzer;
import com.kentcz.cat.services.CatModule;
import com.kentcz.cat.services.LoopService;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = { CatModule.class })
public interface CatComponent {
    Analyzer getAnalyzer();
    LoopService getLoopService();
}
