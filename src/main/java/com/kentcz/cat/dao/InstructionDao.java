package com.kentcz.cat.dao;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface InstructionDao {

    @SqlQuery("SELECT rowId, size, start_rva FROM dd_basic_block WHERE rowId=:id")
    @Mapper(BasicBlockMapper.class)
    DBBasicBlock findById(@Bind("id") int id);

    static final class DBBasicBlock {
        public final int rowId;
        public final int size;
        public final long start_rva;

        public DBBasicBlock(int rowId, int size, long start_rva) {
            this.rowId = rowId;
            this.size = size;
            this.start_rva = start_rva;
        }
    }

    static final class BasicBlockMapper implements ResultSetMapper<DBBasicBlock> {
        @Override
        public DBBasicBlock map(int index, ResultSet r, StatementContext ctx) throws SQLException {
            return new DBBasicBlock(r.getInt("rowId"), r.getInt("size"), r.getLong("start_rva"));
        }
    }
}
