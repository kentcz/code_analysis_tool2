package com.kentcz.cat.dao;

import org.skife.jdbi.v2.DBI;

public class DaoFactory {

    private final DBI dbi;

    public DaoFactory(String sqlitePath) {
        this.dbi = new DBI("jdbc:sqlite:" + sqlitePath);
    }

    public InstructionDao buildInstructionDao() {
        return dbi.onDemand(InstructionDao.class);
    }
}
