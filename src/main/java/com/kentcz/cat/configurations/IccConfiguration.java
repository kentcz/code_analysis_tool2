package com.kentcz.cat.configurations;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class IccConfiguration {
    @NotEmpty
    @JsonProperty("bin-path")
    private final String binPath = null;

    public String getBinPath() { return binPath; }
}
