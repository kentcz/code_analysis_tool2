package com.kentcz.cat.configurations;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class IacaConfiguration {
    @NotNull
    @NotEmpty
    @JsonProperty("bin-path")
    private final String binPath = null;

    @NotNull
    @NotEmpty
    @JsonProperty("lib-path")
    private final String libPath = null;

    public String getBinPath() { return binPath; }
    public String getLibPath() { return libPath; }
}
