package com.kentcz.cat.configurations;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.File;

public class OutputConfiguration {

    @NotEmpty
    @JsonProperty("output-path")
    private final String outputPath = null;

    @NotEmpty
    @JsonProperty("basicblock-dir")
    private final String basicblockPath = null;

    @NotEmpty
    @JsonProperty("iaca-dir")
    private final String iacaPath = null;

    @NotEmpty
    @JsonProperty("objdump-dir")
    private final String dumpPath = null;

    @NotEmpty
    @JsonProperty("loop-dir")
    private final String loopPath = null;

    public String getOutputPath() { return outputPath; }
    public String getIacaPath() { return createDir(iacaPath); }
    public String getIacaPath(String root) { return createDir(String.format("%s/%s", root, iacaPath)); }
    public String getBasicblockPath() { return createDir(basicblockPath); }
    public String getBasicblockPath(String root) { return createDir(String.format("%s/%s", root, basicblockPath)); }
    public String getDumpPath() { return createDir(dumpPath); }
    public String getDumpPath(String root) { return createDir(String.format("%s/%s", root, dumpPath)); }
    public String getLoopPath() { return loopPath; }
    public String getLoopPath(String root) {  return createDir(String.format("%s/%s", root, loopPath)); }

    public String createDir(String dirPath) {
        File dir  = new File(dirPath);
        if (!dir.exists()) {
            try {
                dir.mkdirs();
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        return dirPath;
    }
}
