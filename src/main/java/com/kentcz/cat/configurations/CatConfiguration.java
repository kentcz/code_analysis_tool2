package com.kentcz.cat.configurations;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class CatConfiguration extends Configuration {
    @NotNull
    @Valid
    @JsonProperty("icc")
    private final IccConfiguration iccConfiguration = null;

    @NotNull
    @Valid
    @JsonProperty("objdump")
    private final ObjdumpConfiguration objdumpConfiguration = null;

    @NotNull
    @Valid
    @JsonProperty("iaca")
    private final IacaConfiguration iacaConfiguration = null;

    @NotNull
    @Valid
    @JsonProperty("output")
    private final OutputConfiguration outputConfiguration = null;

    @NotNull
    @Valid
    @JsonProperty("xed")
    private final XedConfiguration xedConfiguration = null;

    public IccConfiguration getIccConfiguration() { return iccConfiguration; }

    public ObjdumpConfiguration getObjdumpConfiguration() { return objdumpConfiguration; }

    public IacaConfiguration getIacaConfiguration() { return iacaConfiguration; }

    public OutputConfiguration getOutputConfiguration() { return outputConfiguration; }

    public XedConfiguration getXedConfiguration() { return xedConfiguration; }
}
