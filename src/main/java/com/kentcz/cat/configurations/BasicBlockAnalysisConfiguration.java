package com.kentcz.cat.configurations;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class BasicBlockAnalysisConfiguration {
    @NotNull
    @NotEmpty
    @JsonProperty("output-path")
    private final String outputPath = null;

    public String getOutputPath() { return outputPath; }
}
