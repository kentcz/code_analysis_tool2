package com.kentcz.cat;

import com.kentcz.cat.configurations.CatConfiguration;
import com.kentcz.cat.services.CatModule;
import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.setup.Bootstrap;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;

public class KernelImportCommand extends ConfiguredCommand<CatConfiguration> {

    public KernelImportCommand() {
        super("import-kernel-command", "Import Kernel Command");
    }


    @Override
    public void configure(Subparser subparser) {
        super.configure(subparser);
        subparser.addArgument("-o", "--output")
                .action(Arguments.store())
                .help("Root of the output directory");
        subparser.addArgument("-i", "--input")
                .action(Arguments.store())
                .help("Path to the kernel objdump file");
        subparser.addArgument("-n", "--shortname")
                .action(Arguments.store())
                .help("Name of the kernel");
    }


    @Override
    protected void run(Bootstrap<CatConfiguration> bootstrap,
                       Namespace namespace,
                       CatConfiguration catConfiguration) throws Exception {

        System.out.println("Stating the Import Kernel Command");
        String outputPath = namespace.getString("output");
        String binFile = namespace.getString("input");
        String shortName = namespace.getString("shortname");
        System.out.println("outputPath=" + outputPath);
        System.out.println("binFile=" + binFile);
        System.out.println("shortName=" + shortName);

        CatComponent catComponent = DaggerCatComponent.builder()
                .catModule(new CatModule("", outputPath, catConfiguration,
                        catConfiguration.getIacaConfiguration(),
                        catConfiguration.getIccConfiguration(),
                        catConfiguration.getObjdumpConfiguration(),
                        catConfiguration.getOutputConfiguration(),
                        catConfiguration.getXedConfiguration()))
                .build();
        catComponent.getAnalyzer().analyzeBasicBlock(binFile,shortName);
    }
}
