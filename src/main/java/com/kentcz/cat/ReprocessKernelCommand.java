package com.kentcz.cat;

import com.kentcz.cat.configurations.CatConfiguration;
import com.kentcz.cat.services.CatModule;
import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.setup.Bootstrap;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;

public class ReprocessKernelCommand extends ConfiguredCommand<CatConfiguration> {

    public ReprocessKernelCommand() {
        super("reprocess-kernel-command", "Reprocess Kernel Command");
    }


    @Override
    public void configure(Subparser subparser) {
        super.configure(subparser);
        subparser.addArgument("-o", "--output")
                .action(Arguments.store())
                .help("Root of the output directory");
        subparser.addArgument("-i", "--input")
                .action(Arguments.store())
                .help("Source file");
        subparser.addArgument("-n", "--shortname")
                .action(Arguments.store())
                .help("Name of the kernel");
    }


    @Override
    protected void run(Bootstrap<CatConfiguration> bootstrap,
                       Namespace namespace,
                       CatConfiguration catConfiguration) throws Exception {

        System.out.println("Stating the Import Kernel Command");
        String outputPath = namespace.getString("output");
        String sourceFile = namespace.getString("input");
        String shortName = namespace.getString("shortname");
        System.out.println("outputPath=" + outputPath);
        System.out.println("sourceFile=" + sourceFile);
        System.out.println("shortName=" + shortName);

        CatComponent catComponent = DaggerCatComponent.builder()
                .catModule(new CatModule("", outputPath, catConfiguration,
                        catConfiguration.getIacaConfiguration(),
                        catConfiguration.getIccConfiguration(),
                        catConfiguration.getObjdumpConfiguration(),
                        catConfiguration.getOutputConfiguration(),
                        catConfiguration.getXedConfiguration()))
                .build();
        catComponent.getAnalyzer().loadSourceFile(sourceFile, shortName);
    }
}
