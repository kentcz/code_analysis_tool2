package com.kentcz.cat.models.xed;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class XedMemoryOp {

    private final int opNum;
    private final String rw;
    private final String base;
    private final String index;
    private final int scale;
    private final int displacement;
    private final int aWidth;

    @JsonCreator
    public XedMemoryOp(@JsonProperty("MemOpNum") int opNum,
                       @JsonProperty("RW") String rw,
                       @JsonProperty("Base") String base,
                       @JsonProperty("Index") String index,
                       @JsonProperty("Scale") int scale,
                       @JsonProperty("Displacement") int displacement,
                       @JsonProperty("AWidth") int aWidth) {
        this.opNum = opNum;
        this.rw = rw;
        this.base = base;
        this.index = index;
        this.scale = scale;
        this.displacement = displacement;
        this.aWidth = aWidth;
    }

    @JsonProperty("MemOpNum")
    public int getOpNum() {
        return opNum;
    }

    @JsonProperty("RW")
    public String getRw() {
        return rw;
    }

    @JsonProperty("Base")
    public String getBase() {
        return base;
    }

    @JsonProperty("Index")
    public String getIndex() { return index; }

    public int getScale() {
        return scale;
    }

    @JsonProperty("Displacement")
    public int getDisplacement() {
        return displacement;
    }

    @JsonProperty("AWidth")
    public int getaWidth() {
        return aWidth;
    }
}
