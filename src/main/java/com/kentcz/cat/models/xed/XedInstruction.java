package com.kentcz.cat.models.xed;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.MoreObjects;
import com.google.common.collect.Lists;
import com.kentcz.cat.models.Instruction;
import com.kentcz.cat.models.MemoryOperand;
import com.kentcz.cat.models.Operand;

import java.util.List;

public class XedInstruction {
    private final String decode;
    private final String instructionClass;
    private final String category;
    private final String isaExt;
    private final String isaSet;
    private final List<XedOperand> operands;
    private final List<XedMemoryOp> memoryOps;
    private final List<String> flagRead;
    private final List<String> flagWrite;
    private final String exceptions;
    private final int memOperandBytes;


    @JsonCreator
    public XedInstruction(@JsonProperty("Decode") String decode,
                          @JsonProperty("Class") String instructionClass,
                          @JsonProperty("Category") String category,
                          @JsonProperty("ISAExt") String isaExt,
                          @JsonProperty("ISASet") String isaSet,
                          @JsonProperty("Operands") List<XedOperand> operands,
                          @JsonProperty("MemoryOps") List<XedMemoryOp> memoryOps,
                          @JsonProperty("Flag_read") List<String> flagRead,
                          @JsonProperty("Flag_write") List<String> flagWrite,
                          @JsonProperty("Exceptions") String exceptions,
                          @JsonProperty("MemopBytes") int memOperandBytes) {
        this.decode = decode;
        this.instructionClass = instructionClass;
        this.category = category;
        this.isaExt = isaExt;
        this.isaSet = isaSet;
        if (operands == null) {
            this.operands = Lists.newArrayList();
        } else {
            this.operands = operands;
        }
        if (memoryOps == null) {
            this.memoryOps = Lists.newArrayList();
        } else {
            this.memoryOps = memoryOps;
        }
        if (flagRead == null) {
            this.flagRead = Lists.newArrayList();
        } else {
            this.flagRead = flagRead;
        }
        if (flagWrite == null) {
            this.flagWrite = Lists.newArrayList();
        } else {
            this.flagWrite = flagWrite;
        }
        this.exceptions = exceptions;
        this.memOperandBytes = memOperandBytes;
    }


    @JsonProperty("Decode")
    public String getDecode() {
        return decode;
    }

    @JsonProperty("Class")
    public String getInstructionClass() {
        return instructionClass;
    }

    @JsonProperty("Category")
    public String getCategory() {
        return category;
    }

    @JsonProperty("ISAExt")
    public String getIsaExt() {
        return isaExt;
    }

    @JsonProperty("ISASet")
    public String getIsaSet() {
        return isaSet;
    }

    @JsonProperty("Operands")
    public List<XedOperand> getOperands() {
        return operands;
    }

    @JsonProperty("MemoryOps")
    public List<XedMemoryOp> getMemoryOps() {
        return memoryOps;
    }

    @JsonProperty("Exceptions")
    public String getExceptions() {
        return exceptions;
    }

    @JsonProperty("Flag_read")
    public List<String> getFlagRead() {
        return flagRead;
    }

    @JsonProperty("Flag_write")
    public List<String> getFlagWrite() {
        return flagWrite;
    }

    @JsonProperty("MemopBytes")
    public int getMemOperandBytes() {
        return memOperandBytes;
    }

    public static XedInstruction parseString(String s) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(s, XedInstruction.class);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("decode", decode)
                .add("instructionClass", instructionClass)
                .add("category", category)
                .add("isaExt", isaExt)
                .add("isaSet", isaSet)
                .add("operands", operands)
                .add("memoryOps", memoryOps)
                .add("flagRead", flagRead)
                .add("flagWrite", flagWrite)
                .add("memOperandBytes", memOperandBytes)
                .toString();
    }

    public Instruction toInstruction() {
        List<Operand> operands = Lists.newArrayList();
        int memoryOpCount = 0;
        for (XedOperand xedOperand : this.operands) {
            if (xedOperand.isMemory() || xedOperand.isAgen()) {
                XedMemoryOp xedMemoryOp = this.getMemoryOps().get(memoryOpCount);
                MemoryOperand operand = new MemoryOperand(
                        xedOperand.getType(),
                        xedOperand.isExplicit(),
                        xedOperand.getElemSize(),
                        xedOperand.getDetails(),
                        xedOperand.isRead(),
                        xedOperand.isWrite(),
                        xedMemoryOp.getBase(),
                        xedMemoryOp.getIndex(),
                        xedMemoryOp.getScale(),
                        xedMemoryOp.getDisplacement(),
                        xedMemoryOp.getaWidth(),
                        xedOperand.getBits());
                operands.add(operand);
                memoryOpCount++;
            } else { Operand operand = new Operand(
                        xedOperand.getType(),
                        xedOperand.isExplicit() || xedOperand.isImplicit(),
                        xedOperand.getElemSize(),
                        xedOperand.getDetails(),
                        xedOperand.isRead(),
                        xedOperand.isWrite());
                operands.add(operand);
            }
        }
        return new Instruction(this.getInstructionClass().toLowerCase(),
                operands, this.getFlagRead(), this.getFlagWrite());
    }

    public List<String> readRegisters() {
        List<String> registers = Lists.newArrayList();
        for (XedOperand operand : operands) {
            if (operand.isRegister() && operand.isRead()) {
                registers.add(operand.getDetails());
            }
        }
        return registers;
    }

    public List<String> writeRegisters() {
        List<String> registers = Lists.newArrayList();
        for (XedOperand operand : operands) {
            if (operand.isRegister() && operand.isWrite()) {
                registers.add(operand.getDetails());
            }
        }
        return registers;
    }
}
