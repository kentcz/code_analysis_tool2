package com.kentcz.cat.models.xed;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class XedOperand {
    private final int opNum;
    private final String type;
    private final String details;
    private final String vis;
    private final String rw;
    private final int bits;
    private final int numElem;
    private final int elemSize;
    private final String elemType;

    @JsonCreator
    public XedOperand(@JsonProperty("OpNum") int opNum,
                      @JsonProperty("Type") String type,
                      @JsonProperty("Details") String details,
                      @JsonProperty("VIS") String vis,
                      @JsonProperty("RW") String rw,
                      @JsonProperty("Bits") int bits,
                      @JsonProperty("NumElem") int numElem,
                      @JsonProperty("ElemSize") int elemSize,
                      @JsonProperty("ElemType") String elemType) {
        this.opNum = opNum;
        this.type = type;
        this.details = details;
        this.vis = vis;
        this.rw = rw;
        this.bits = bits;
        this.numElem = numElem;
        this.elemSize = elemSize;
        this.elemType = elemType;
    }

    @JsonProperty("OpNum")
    public int getOpNum() {
        return opNum;
    }

    @JsonProperty("Type")
    public String getType() {
        return type;
    }

    @JsonProperty("Details")
    public String getDetails() {
        return details;
    }

    @JsonProperty("VIS")
    public String getVis() {
        return vis;
    }

    @JsonProperty("RW")
    public String getRw() {
        return rw;
    }

    @JsonProperty("Bits")
    public int getBits() {
        return bits;
    }

    @JsonProperty("NumElem")
    public int getNumElem() {
        return numElem;
    }

    @JsonProperty("ElemSize")
    public int getElemSize() {
        return elemSize;
    }

    @JsonProperty("ElemType")
    public String getElemType() {
        return elemType;
    }


    boolean isRead() {
        return rw.equals("R") || rw.equals("RW");
    }

    boolean isWrite() { return rw.equals("W") || rw.equals("RW"); }

    boolean isRegister() {
        return (type.substring(0,3).equals("REG"));
    }

    boolean isMemory() {
        return (type.substring(0,3).equals("MEM"));
    }

    boolean isImmediate() {
        return (type.substring(0,3).equals("IMM"));
    }

    boolean isAgen() {
        return (type.substring(0,4).equals("AGEN"));
    }

    boolean isExplicit() { return vis.equals("EXPLICIT"); }

    boolean isImplicit() { return vis.equals("IMPLICIT"); }
}
