package com.kentcz.cat.models.objdump;

import java.util.List;

public class ObjdumpInstruction {
    private final String bytes;
    private final String asm;
    private final long address;

    public ObjdumpInstruction(long address, String bytes, String asm) {
        this.address = address;
        this.asm = asm;
        this.bytes = bytes;
    }

    public String getBytes() {
        return bytes;
    }

    public String getAsm() {
        return asm;
    }

    public long getAddress() {
        return address;
    }
}
