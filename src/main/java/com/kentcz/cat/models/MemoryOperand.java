package com.kentcz.cat.models;

import com.google.common.collect.Lists;

import java.util.List;

public class MemoryOperand extends Operand {

    private final String base;
    private final String index;
    private final int scale;
    private final int displacement;
    private final int width;
    private final int bits;

    public MemoryOperand(String type,
                         boolean explicit,
                         int elementSize,
                         String details,
                         boolean read,
                         boolean write,
                         String base,
                         String index,
                         int scale,
                         int displacement,
                         int width,
                         int bits) {
        super(type, explicit, elementSize, details, read, write);
        this.base = base;
        this.index = index;
        this.scale = scale;
        this.displacement = displacement;
        this.width = width;
        this.bits= bits;
    }

    public String ptrToString(int bits) {
        if (bits == 0) {
            return "";
        }
        if (bits == 8) {
            return "BYTE PTR ";
        }
        if (bits == 16) {
            return "WORD PTR ";
        }
        if (bits == 32) {
            return "DWORD PTR ";
        }
        if (bits == 64) {
            return "QWORD PTR ";
        }
        if (bits == 128) {
            return "XMMWORD PTR ";
        }
        if (bits == 256) {
            return "YMMWORD PTR ";
        }
        System.out.println("Error: Input to MemoryOperand.prtToString(bits=" + bits +") failed");
        return "";
    }

    public String getBase() {
        return base;
    }

    public String getIndex() {
        return index;
    }

    public int getScale() {
        return scale;
    }

    public int getDisplacement() {
        return displacement;
    }

    public int getWidth() {
        return width;
    }

    public int getBits() {
        return bits;
    }

    public boolean hasBase() {
        return base != null && base.length() > 0;
    }

    public boolean hasIndex() {
        return index != null && index.length() > 0;
    }

    // Example: "XMMWORD PTR [rbx+r15*8+0x28]"
    public String encodeAddress() {

        StringBuilder address = new StringBuilder();
        if (hasBase()) {
            address.append(base.toLowerCase());
        }
        if (hasIndex()) {
            if (hasBase()) {
                address.append("+");
            }
            address.append(index.toLowerCase());
        }
        if (scale != 0) {
            address.append("*" + Integer.toString(scale));
        }
        if (displacement != 0) {

            // Handle negative displacement
            int val = displacement;
            String add = "+";
            if (val < 0) {
                val = val * -1;
                add = "-";
            }

            if (address.length() > 0) {
                address.append(add);
            }
            address.append("0x" + Integer.toHexString(val));
        }

        if (isAgen()) {
            return String.format("[%s]", address.toString());
        }
        return String.format("%s[%s]", ptrToString(bits), address.toString());
    }

    @Override
    public String toAssembly() {
        return encodeAddress();
    }

    public boolean hasRipBase() { return hasBase() && (base.equals("RIP") || base.equals("rip")); }

    @Override
    public Operand removeDisplacement() {
        return new MemoryOperand(this.getType(),
                         this.isExplicit(),
                         this.getElementSize(),
                         this.getDetails(),
                         this.isRead(),
                         this.isWrite(),
                         this.getBase(),
                         this.getIndex(),
                         this.getScale(),
                         0,
                         this.getWidth(),
                         this.getBits());
    }


    public List<String> readRegisters() {
        List<String> readRegisters = Lists.newArrayList();
        if (hasBase()) {
            readRegisters.add(base);
        }
        if (hasIndex()) {
            readRegisters.add(index);
        }
        return readRegisters;
    }

     public List<String> writeRegisters() {
         return Lists.newArrayList();
     }
}