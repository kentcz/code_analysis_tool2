package com.kentcz.cat.models.opcodemix;

public class OpcodeSample {
    private final int id;
    private final String opcode;
    private final long count;
    private final long predicatedCount;

    public OpcodeSample(int id, String opcode, long count, long predicatedCount) {
        this.id = id;
        this.opcode = opcode;
        this.count = count;
        this.predicatedCount = predicatedCount;
    }

    public int getId() {
        return id;
    }

    public String getOpcode() {
        return opcode;
    }

    public long getCount() {
        return count;
    }

    public long getPredicatedCount() {
        return predicatedCount;
    }

    public long countTotal() {
        return count + predicatedCount;
    }

    public boolean isTotal() {
        return opcode.equals("*total");
    }

    public boolean isStack() {
        return opcode.equals("*stack-read")
                || opcode.equals("*stack-write");
    }

    public boolean isMemoryRead() {
        return opcode.startsWith("*mem-read");
    }
    public boolean isMemoryWrite() {
        return opcode.startsWith("*mem-write");
    }
    public boolean isMemoryRead1() {
        return opcode.equals("*mem-read-1");
    }
    public boolean isMemoryRead2() {
        return opcode.equals("*mem-read-2");
    }
    public boolean isMemoryRead4() {
        return opcode.equals("*mem-read-4");
    }
    public boolean isMemoryRead8() {
        return opcode.equals("*mem-read-8");
    }
    public boolean isMemoryRead16() {
        return opcode.equals("*mem-read-16");
    }
    public boolean isMemoryRead32() {
        return opcode.equals("*mem-read-32");
    }

    public boolean isAVX() {
        return opcode.startsWith("V") || isSSE();
    }

    public boolean isSSE() {
         return (opcode.startsWith("MUL")
                 || opcode.startsWith("DIV")
                 || opcode.startsWith("ADD")
                 || opcode.startsWith("SUB"))
                 && (opcode.endsWith("PD")
                 || opcode.endsWith("SD")
                 || opcode.endsWith("SS")
                 || opcode.endsWith("PS"));
    }

    public boolean isAVXfp() {
        return opcode.startsWith("VMUL")
                || opcode.startsWith("VDIV")
                || opcode.startsWith("VADD")
                || opcode.startsWith("VSUB")
                || opcode.startsWith("VDP")
                || opcode.startsWith("VHADD")
                || opcode.startsWith("VHSUB")
                || isSSE();
    }

    public boolean isVectorized() {
        return (isAVX() || isSSE()) && (opcode.endsWith("PD")
                || opcode.endsWith("PS"));
    }

    public boolean isBranch() {
        return opcode.equals("JB")
                || opcode.equals("JBE")
                || opcode.equals("JL")
                || opcode.equals("JLE")
                || opcode.equals("JMP")
                || opcode.equals("JNB")
                || opcode.equals("JNBE")
                || opcode.equals("JNL")
                || opcode.equals("JNLE")
                || opcode.equals("JNP")
                || opcode.equals("JNS")
                || opcode.equals("JNZ")
                || opcode.equals("JP")
                || opcode.equals("JS")
                || opcode.equals("JZ");
    }
}
