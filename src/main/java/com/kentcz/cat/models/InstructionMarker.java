package com.kentcz.cat.models;

import com.google.common.base.MoreObjects;

public class InstructionMarker {

    private long address;
    private String bytes;
    private String assembly;

    public long getAddress() {
        return address;
    }

    public String getBytes() {
        return bytes;
    }

    public String getAssembly() {
        return assembly;
    }

    public String getSanitizedAssembly() {
        return sanitizeAssembly(assembly);
    }

    private String sanitizeAssembly(String assemblyString) {
        String[] words = assemblyString.split("\\s+");
        String asm = "";
        for (int i = 0; i < words.length; i++) {
            char first_char = words[i].charAt(0);
            if (first_char == '#') { return asm; }
            if (first_char != '<') {
                if (i > 0) {
                    asm += " ";
                }
                asm += words[i];
            }
        }
        return asm;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("address", address)
                .add("bytes", bytes)
                .add("assembly", assembly)
                .toString();
    }

}
