package com.kentcz.cat.models;

public class VariableOperand extends Operand {

    private final String variableName;

    public VariableOperand(String type,
                           boolean explicit,
                           int elementSize,
                           String details,
                           boolean read,
                           boolean write,
                           String variableName) {
        super(type, explicit, elementSize, details, read, write);
        this.variableName = variableName;
    }

    @Override
    public String toAssembly() {
        return variableName;
    }

    /**
    @Override
    public Operand removeDisplacement() {
        return this;
    }
    **/

    public static VariableOperand replaceMemoryOperand(MemoryOperand operand, String variableName) {
        return new VariableOperand(operand.getType(),
                operand.isExplicit(),
                operand.getElementSize(),
                operand.getDetails(),
                operand.isRead(),
                operand.isWrite(),
                variableName);
    }
}
