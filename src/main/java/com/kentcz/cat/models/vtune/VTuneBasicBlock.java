package com.kentcz.cat.models.vtune;

import com.google.common.base.MoreObjects;
import com.kentcz.cat.models.Instruction;

import java.util.ArrayList;
import java.util.List;

public class VTuneBasicBlock {
    private final long rowId;
    private final long startAddress;
    private final long lastInsAddress;
    private final long displayAddress;
    private final int size;
    private final long jumpTargetId;
    private final int branchType;
    private final double percentTime;
    private final String binName;
    private final String binPath;
    private List<VTuneSourceLocation> sources;
    private List<Instruction> instructions;

    public VTuneBasicBlock(long rowId,
                           long startAddress,
                           long lastInsAddress,
                           long displayAddress,
                           int size,
                           long jumpTargetId,
                           int branchType,
                           double percentTime,
                           String binName,
                           String binPath) {
        this.rowId = rowId;
        this.startAddress = startAddress;
        this.lastInsAddress = lastInsAddress;
        this.displayAddress = displayAddress;
        this.size = size;
        this.jumpTargetId = jumpTargetId;
        this.branchType = branchType;
        this.percentTime = percentTime;
        this.binName = binName;
        this.binPath = binPath;
    }

    public long getRowId() {
        return rowId;
    }

    public long getStartAddress() {
        return startAddress;
    }

    public long getLastInsAddress() {
        return lastInsAddress;
    }

    public long getDisplayAddress() {
        return displayAddress;
    }

    public int getSize() {
        return size;
    }

    public long getJumpTargetId() {
        return jumpTargetId;
    }

    public int getBranchType() {
        return branchType;
    }

    public double getPercentTime() {
        return percentTime;
    }

    public String getBinName() {
        return binName;
    }

    public String getBinPath() {
        return binPath;
    }

    public List<VTuneSourceLocation> getSources() {
        return sources;
    }

    public List<Instruction> getInstructions() {
        return instructions;
    }

    public void setSources(List<VTuneSourceLocation> sources) {
        this.sources = sources;
    }

    public void setInstructions(ArrayList<Instruction> instructions) {
        this.instructions = instructions;
    }

    public long getDisplayLastAddress() { return displayAddress + (lastInsAddress - startAddress); }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("rowId", rowId)
                .add("startAddress", startAddress)
                .add("lastInsAddress", lastInsAddress)
                .add("displayAddress", displayAddress)
                .add("size", size)
                .add("jumpTargetId", jumpTargetId)
                .add("branchType", branchType)
                .add("percentTime", percentTime)
                .add("binName", binName)
                .add("binPath", binPath)
                .add("sources", sources)
                .toString();
    }
}
