package com.kentcz.cat.models.vtune;

import com.google.common.base.MoreObjects;

public class VTuneSourceLocation {
    private final long sourceLocationId;
    private final String sourceFile;
    private final String sourceLine;
    private final String sourcePath;
    private final String functionFullName;
    private final String functionName;

    public VTuneSourceLocation(long sourceLocationId,
                               String sourceFile,
                               String sourceLine,
                               String sourcePath,
                               String functionFullName,
                               String functionName) {
        this.sourceLocationId = sourceLocationId;
        this.sourceFile = sourceFile;
        this.sourceLine = sourceLine;
        this.sourcePath = sourcePath;
        this.functionFullName = functionFullName;
        this.functionName = functionName;
    }

    public long getSourceLocationId() {
        return sourceLocationId;
    }

    public String getSourceFile() {
        return sourceFile;
    }

    public String getSourceLine() {
        return sourceLine;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public String getFunctionFullName() {
        return functionFullName;
    }

    public String getFunctionName() {
        return functionName;
    }

    public String toCsv() {
        return sourcePath + ":" + sourceLine + "\t" + functionFullName;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("sourceLocationId", sourceLocationId)
                .add("sourceFile", sourceFile)
                .add("sourceLine", sourceLine)
                .add("sourcePath", sourcePath)
                .add("fucntionFullName", functionFullName)
                .add("functionName", functionName)
                .toString();
    }
}
