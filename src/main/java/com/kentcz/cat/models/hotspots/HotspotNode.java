package com.kentcz.cat.models.hotspots;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Lists;
import com.kentcz.cat.resources.HotspotParser;

import java.util.List;

public class HotspotNode {
    private final double totalTime;
    private final double selfTime;
    private final String label;
    private final String module;
    private final int level;
    private final List<HotspotNode> children = Lists.newArrayList();


    public HotspotNode(double totalTime, double selfTime, String label, String module, int level) {
        this.totalTime = totalTime;
        this.selfTime = selfTime;
        this.label = label;
        this.module = module;
        this.level = level;
    }

    public double getTotalTime() {
        return totalTime;
    }

    public double getSelfTime() {
        return selfTime;
    }

    public String getLabel() {
        return label;
    }

    public String getModule() {
        return module;
    }

    public List<HotspotNode> getChildren() {
        return children;
    }

    public int getLevel() {
        return level;
    }

    public void addChild(HotspotNode child) { children.add(child); }

    public boolean isLeaf() { return children == null || children.size() == 0; }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("level", level)
                .add("label", label)
                .add("module", module)
                .add("totalTime", totalTime)
                .add("selfTime", selfTime)
                .add("children", children)
                .toString();
    }

}
