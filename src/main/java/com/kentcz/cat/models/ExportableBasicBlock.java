package com.kentcz.cat.models;

import java.util.List;

public class ExportableBasicBlock {

    private final List<String> arrayVariables;
    private final List<String> ripVariables;
    private final List<Instruction> instructions;
    private final String iterationRegister;
    private final String loopCountRegister;
    private final int arraySize;

    public ExportableBasicBlock(List<String> arrayVariables,
                                List<String> ripVariables,
                                List<Instruction> instructions,
                                String iterationRegister,
                                String loopCountRegister,
                                int arraySize) {
        this.arrayVariables = arrayVariables;
        this.ripVariables = ripVariables;
        this.instructions = instructions;
        this.iterationRegister = iterationRegister;
        this.loopCountRegister = loopCountRegister;
        this.arraySize = arraySize;
    }

    public String getLoopCountRegister() {
        return loopCountRegister;
    }

    public int getArraySize() {
        return arraySize;
    }

    public List<String> getArrayVariables() {
        return arrayVariables;
    }

    public List<String> getRipVariables() {
        return ripVariables;
    }

    public List<Instruction> getInstructions() {
        return instructions;
    }

    public String getIterationRegister() {
        return iterationRegister;
    }
}
