package com.kentcz.cat.models;

public class BlockLocation {
    private final String binPath;
    private final String binName;
    private final long startAddress;
    private final long endAddress;

    public BlockLocation(String binPath, String binName, long startAddress, int numBytes) {
        this.binPath = binPath;
        this.binName = binName;
        this.startAddress = startAddress;
        this.endAddress = startAddress + numBytes;
    }

    public String getBinPath() {
        return binPath;
    }

    public String getBinName() {
        return binName;
    }

    public long getStartAddress() {
        return startAddress;
    }

    public long getEndAddress() {
        return endAddress;
    }
}
