package com.kentcz.cat.models;

import com.google.common.collect.Lists;

import java.util.List;

public class Operand {
    private String type;
    private boolean explicit;
    private int elementSize;
    private String details;
    private boolean read;
    private boolean write;

    public Operand(String type, boolean explicit, int elementSize, String details, boolean read, boolean write) {
        this.type = type;
        this.explicit = explicit;
        this.elementSize = elementSize;
        this.details = details;
        this.read = read;
        this.write = write;
    }

    public String getType() {
        return type;
    }

    public boolean isExplicit() {
        return explicit;
    }

    public int getElementSize() {
        return elementSize;
    }

    public String getDetails() {
        return details;
    }

    public boolean isRead() {
        return read;
    }

    public boolean isWrite() {
        return write;
    }

    public boolean isRegister() {
        return (type.substring(0,3).equals("REG"));
    }

    public boolean isMemory() {
        return (type.substring(0,3).equals("MEM"));
    }

    public boolean isImmediate() {
        return (type.substring(0,3).equals("IMM"));
    }

    public boolean isAgen() {
        return (type.substring(0,4).equals("AGEN"));
    }

    public boolean hasRipBase() { return false; }

    public boolean isSIMD() {
        return isRegister()
                && (details.toLowerCase().startsWith("xmm")
                || details.toLowerCase().startsWith("ymm"));
    }

    public String toAssembly() {

        if (isImmediate()) {
            long val = Long.parseLong(details);
            return "0x" + Long.toHexString(val);
        }
        if (isRegister()) {
            return details.toLowerCase();
        }
        return new String(details);
    }

    public List<String> readRegisters() {
        List<String> readRegisters = Lists.newArrayList();
        if (isRegister() && isRead() && !details.equals("rflags")) {
            readRegisters.add(details);
        }
        return readRegisters;
    }

     public List<String> writeRegisters() {
         List<String> readRegisters = Lists.newArrayList();
         if (isRegister() && isWrite() && !details.equals("rflags")) {
             readRegisters.add(details);
         }
         return readRegisters;
     }

    public Operand removeDisplacement() {
        /**
        return new Operand(type,
                explicit,
                elementSize,
                details,
                read,
                write); **/
        return this;
    }

    public Operand replaceSIMDRegister(String simdRegister) {
        return new Operand(type,
                explicit,
                elementSize,
                simdRegister,
                read,
                write);
    }


}
