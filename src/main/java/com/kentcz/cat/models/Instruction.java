package com.kentcz.cat.models;

import com.google.common.collect.Lists;

import java.util.List;

public class Instruction {
    private String operation;
    private List<Operand> operands;
    private List<String> readFlags;
    private List<String> writeFlags;

    public Instruction(String operation, List<Operand> operands, List<String> readFlags, List<String> writeFlags) {
        this.operation = operation;
        this.operands = operands;
        this.readFlags = readFlags;
        this.writeFlags = writeFlags;
    }

    public String getOperation() {
        return operation;
    }

    public List<Operand> getOperands() {
        return operands;
    }

    public List<String> getReadFlags() {
        return readFlags;
    }

    public List<String> getWriteFlags() {
        return writeFlags;
    }

    public String toAssembly() {
        StringBuilder assembly = new StringBuilder();
        assembly.append(operation);

        int displayCount = 0;
        for (Operand operand : operands) {

            if (operand.isExplicit()) {
                if (displayCount > 0) {
                    assembly.append(",");
                } else {
                    assembly.append(" ");
                }
                displayCount++;
                assembly.append(operand.toAssembly());
            }
        }
        return assembly.toString();
    }

    public boolean hasRipBase() {
        for (Operand operand : operands) {
            if (operand.hasRipBase()) { return true; }
        }
        return false;
    }

    public Instruction replaceOperands(List<Operand> newOperands) {
        return new Instruction(operation, newOperands, readFlags, writeFlags);
    }

    public Instruction replaceRip(String replacementVariable) {
        List<Operand> newOperands = Lists.newArrayList();
        for (Operand operand : operands) {
            if (operand.hasRipBase()) {
                newOperands.add(VariableOperand.replaceMemoryOperand((MemoryOperand) operand, replacementVariable));
            } else {
                newOperands.add(operand);
            }
        }
        return replaceOperands(newOperands);
    }

    public List<String> readRegisters() {
        List<String> readRegisters = Lists.newArrayList();
        for (Operand operand : operands) {
            for (String register : operand.readRegisters()) {
                readRegisters.add(register);
            }
        }
        return readRegisters;
    }

    public List<String> writeRegisters() {
        List<String> writeRegisters = Lists.newArrayList();
        for (Operand operand : operands) {
            for (String register : operand.writeRegisters()) {
                writeRegisters.add(register);
            }
        }
        return writeRegisters;
    }

    public boolean isBranch() {
        return operation.equals("JB")
                || operation.equals("JBE")
                || operation.equals("JL")
                || operation.equals("JLE")
                || operation.equals("JMP")
                || operation.equals("JNB")
                || operation.equals("JNBE")
                || operation.equals("JNL")
                || operation.equals("JNLE")
                || operation.equals("JNP")
                || operation.equals("JNS")
                || operation.equals("JNZ")
                || operation.equals("JP")
                || operation.equals("JS")
                || operation.equals("JZ");
    }


    public Instruction removeDisplacement() {
        List<Operand> newOperands = Lists.newArrayList();
        for (Operand operand : operands) {
            newOperands.add(operand.removeDisplacement());
        }
        return new Instruction(operation, newOperands, readFlags, writeFlags);
    }
}
