package com.kentcz.cat.models.iaca;

import com.google.common.base.MoreObjects;

import java.util.ArrayList;
import java.util.List;

public class IacaInstruction {
    private final int numUops;
    private final String asm;
    private final double port0;
    private final double port0D;
    private final double port1;
    private final double port2;
    private final double port2D;
    private final double port3;
    private final double port3D;
    private final double port4;
    private final double port5;
    private final double port6;
    private final double port7;

    private final List<IacaInstruction> dependencies = new ArrayList<>();

    public IacaInstruction(int numUops, String asm,
                           double port0,
                           double port0D,
                           double port1,
                           double port2,
                           double port2D,
                           double port3,
                           double port3D,
                           double port4,
                           double port5,
                           double port6,
                           double port7) {
        this.asm = asm;
        this.numUops = numUops;
        this.port0D = port0D;
        this.port0 = port0;
        this.port1 = port1;
        this.port2 = port2;
        this.port2D = port2D;
        this.port3 = port3;
        this.port3D = port3D;
        this.port4 = port4;
        this.port5 = port5;
        this.port6 = port6;
        this.port7 = port7;
    }

    public int getNumUops() {
        return numUops;
    }

    public String getAsm() {
        return asm;
    }

    public double getPort0() {
        return port0;
    }

    public double getPort0D() {
        return port0D;
    }

    public double getPort1() {
        return port1;
    }

    public double getPort2() {
        return port2;
    }

    public double getPort2D() {
        return port2D;
    }

    public double getPort3() {
        return port3;
    }

    public double getPort3D() {
        return port3D;
    }

    public double getPort4() {
        return port4;
    }

    public double getPort5() {
        return port5;
    }

    public double getPort6() {
        return port6;
    }

    public double getPort7() {
        return port7;
    }

    public List<IacaInstruction> getDependencies() {
        return dependencies;
    }

    public void addDependency(IacaInstruction iacaInstruction) {
        this.dependencies.add(iacaInstruction);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("port0", port0)
                .add("port0D", port0D)
                .add("port1", port1)
                .add("port2", port2)
                .add("port2D", port2D)
                .add("port3", port3)
                .add("port3D", port3D)
                .add("port4", port4)
                .add("port5", port5)
                .add("port6", port6)
                .add("port7", port7)
                .add("numUops", numUops)
                .add("asm", asm)
                .toString();
    }
}
