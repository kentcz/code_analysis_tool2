package com.kentcz.cat.models.iaca;

import com.google.common.base.MoreObjects;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class IacaBlock {
    private double throughput;
    private List<IacaInstruction> instructions;

    public IacaBlock(double throughput, List<IacaInstruction> instructions) {
        this.throughput = throughput;
        this.instructions = instructions;
        checkNotNull(instructions);
        for (IacaInstruction iacaInstruction : instructions) { checkNotNull(iacaInstruction); }
    }

    public double getThroughput() {
        return throughput;
    }

    public List<IacaInstruction> getInstructions() {
        return instructions;
    }

    public int numUops() {
        int numUops = 0;
        for (IacaInstruction iacaInstruction : instructions) {
            numUops += iacaInstruction.getNumUops();
        }
        return numUops;
    }

    public double port0() {
        double port = 0;
        for (IacaInstruction iacaInstruction : instructions) { port += iacaInstruction.getPort0(); }
        return port;
    }

    public double port0D() {
        double port = 0;
        for (IacaInstruction iacaInstruction : instructions) { port += iacaInstruction.getPort0D(); }
        return port;
    }

    public double port1() {
        double port = 0;
        for (IacaInstruction iacaInstruction : instructions) { port += iacaInstruction.getPort1(); }
        return port;
    }

    public double port2() {
        double port = 0;
        for (IacaInstruction iacaInstruction : instructions) { port += iacaInstruction.getPort2(); }
        return port;
    }

    public double port2D() {
        double port = 0;
        for (IacaInstruction iacaInstruction : instructions) { port += iacaInstruction.getPort2D(); }
        return port;
    }

    public double port3() {
        double port = 0;
        for (IacaInstruction iacaInstruction : instructions) { port += iacaInstruction.getPort3(); }
        return port;
    }

    public double port3D() {
        double port = 0;
        for (IacaInstruction iacaInstruction : instructions) { port += iacaInstruction.getPort3D(); }
        return port;
    }

    public double port4() {
        double port = 0;
        for (IacaInstruction iacaInstruction : instructions) { port += iacaInstruction.getPort4(); }
        return port;
    }

    public double port5() {
        double port = 0;
        for (IacaInstruction iacaInstruction : instructions) { port += iacaInstruction.getPort5(); }
        return port;
    }

    public double port6() {
        double port = 0;
        for (IacaInstruction iacaInstruction : instructions) { port += iacaInstruction.getPort6(); }
        return port;
    }

    public double port7() {
        double port = 0;
        for (IacaInstruction iacaInstruction : instructions) { port += iacaInstruction.getPort7(); }
        return port;
    }

    @Override
    public String toString() {
        MoreObjects.ToStringHelper helper = MoreObjects.toStringHelper(this);
        for (int i = 0; i < instructions.size(); i++) {
            helper.add("instructions[" + i + "]",instructions.get(i));
        }
        helper.add("throughput",throughput);
        return helper.toString();
    }
}
