package com.kentcz.cat.models.iaca;

public class DiagraphEdge {
    private final int parentId;
    private final int childId;

    public DiagraphEdge(int parentId, int childId) {
        this.parentId = parentId;
        this.childId = childId;
    }

    public int getParentId() {
        return parentId;
    }

    public int getChildId() {
        return childId;
    }
}
