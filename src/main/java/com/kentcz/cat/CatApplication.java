package com.kentcz.cat;

import com.kentcz.cat.configurations.CatConfiguration;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class CatApplication extends Application<CatConfiguration> {

    public static void main(String[] args) throws Exception {
        new CatApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<CatConfiguration> bootstrap ) {
        bootstrap.addCommand(new InstructionDecodeCommand());
        bootstrap.addCommand(new ReprocessKernelCommand());
        bootstrap.addCommand(new DumpLoopsCommand());
        bootstrap.addCommand(new AnalyzeCommand());
        bootstrap.addCommand(new KernelImportCommand());
        bootstrap.addCommand(new HotspotCommand());
        bootstrap.addCommand(new OpcodeMixCommand());
    }

    @Override
    public void run(CatConfiguration catConfiguration, Environment env) throws Exception {
    }
}
