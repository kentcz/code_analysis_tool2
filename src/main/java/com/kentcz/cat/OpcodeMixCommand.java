package com.kentcz.cat;

import com.kentcz.cat.configurations.CatConfiguration;
import com.kentcz.cat.models.opcodemix.OpcodeSample;
import com.kentcz.cat.services.OpcodeMixParserService;
import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.setup.Bootstrap;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;

import java.util.List;

public class OpcodeMixCommand extends ConfiguredCommand<CatConfiguration> {
    public OpcodeMixCommand() {
        super("opcodemix-command", "Analyze opcodemix report");
    }

    @Override
    public void configure(Subparser subparser) {
        super.configure(subparser);
        subparser.addArgument("-i", "--input")
                .action(Arguments.store())
                .help("The opcodemix.out file");
    }

    @Override
    protected void run(Bootstrap<CatConfiguration> bootstrap,
                       Namespace namespace,
                       CatConfiguration catConfiguration) throws Exception {

        String inputFile = namespace.getString("input");

        OpcodeMixParserService parser = new OpcodeMixParserService();
        List<OpcodeSample> samples = parser.parseFile(inputFile);
        String csv = String.format("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s",
                parser.countAVX(samples),
                parser.countVectorized(samples),
                parser.countAVXfp(samples),
                parser.countMops(samples),
                parser.countReads(samples),
                parser.countReads16(samples),
                parser.countBranches(samples),
                parser.countStack(samples));
        System.out.println(csv);
    }
}
