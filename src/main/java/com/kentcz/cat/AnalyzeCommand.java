package com.kentcz.cat;


import com.kentcz.cat.configurations.CatConfiguration;
import com.kentcz.cat.configurations.IacaConfiguration;
import com.kentcz.cat.configurations.IccConfiguration;
import com.kentcz.cat.configurations.OutputConfiguration;
import com.kentcz.cat.dao.JDBIModule;
import com.kentcz.cat.services.CatModule;
import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.cli.EnvironmentCommand;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import net.sourceforge.argparse4j.impl.Arguments;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;

public class AnalyzeCommand extends ConfiguredCommand<CatConfiguration> {
    public AnalyzeCommand() {
        super("analyze-command", "Analyze Command");
    }


    @Override
    public void configure(Subparser subparser) {
        super.configure(subparser);
        subparser.addArgument("-o", "--output")
                .action(Arguments.store())
                .help("Root of the output directory");
        subparser.addArgument("-i", "--input")
                .action(Arguments.store())
                .help("Path to the sqlite database file");
    }

    //protected void run(Bootstrap<CatConfiguration> bootstrap,

    @Override
    protected void run(Bootstrap<CatConfiguration> bootstrap,
                       Namespace namespace,
                       CatConfiguration catConfiguration) throws Exception {

        System.out.println("Stating the Analyze Command");
        String outputPath = namespace.getString("output");
        String dbFile = namespace.getString("input");
        //String dbFile = (String) (namespace.getList("input").get(0));
        System.out.println("outputPath=" + outputPath);
        System.out.println("dbFile=" + dbFile);

        CatComponent catComponent = DaggerCatComponent.builder()
                .catModule(new CatModule(dbFile, outputPath, catConfiguration,
                        catConfiguration.getIacaConfiguration(),
                        catConfiguration.getIccConfiguration(),
                        catConfiguration.getObjdumpConfiguration(),
                        catConfiguration.getOutputConfiguration(),
                        catConfiguration.getXedConfiguration()))
                .build();

        catComponent.getAnalyzer().analyzeBasicBlocks();
    }
}