#mvn clean package && java -jar target/CodeAnalysisTool-1.0-SNAPSHOT.jar analyze-command -o ./ -i ./output.txt config.yaml
#mvn clean package && java -jar target/CodeAnalysisTool-1.0-SNAPSHOT.jar hotspot-command -i ~/hotspots_td.txt -o ~/output.test config.yaml
#mvn clean package && java -jar target/CodeAnalysisTool-1.0-SNAPSHOT.jar opcodemix-command -i ~/scratch/opcodemix/proxy_apps/CoEVP/opcodemix.out config.yaml


#mvn clean package && java -jar target/CodeAnalysisTool-1.0-SNAPSHOT.jar analyze-command -o /home/kentcz/scratch/tmp_codeanalysis/ -i /home/kentcz/research/proxy_apps/output/XSBench/hotspots/sqlite-db/dicer.db config.yaml


CA_ROOT_PATH="/home/kentcz/scratch/tmp_codeanalysis/"
KERNEL="XSBench_4210720_4210732"

rm application.log
#mvn clean package && java -jar target/CodeAnalysisTool-1.0-SNAPSHOT.jar import-kernel-command -o ${CA_ROOT_PATH} -i ${CA_ROOT_PATH}/objdumps/${KERNEL}.bin -n ${KERNEL} config.yaml

#mvn clean package && java -jar target/CodeAnalysisTool-1.0-SNAPSHOT.jar reprocess-kernel-command -o ${CA_ROOT_PATH} -i ${CA_ROOT_PATH}/objdumps/${KERNEL}.c -n ${KERNEL} config.yaml

mvn clean package && java -jar target/CodeAnalysisTool-1.0-SNAPSHOT.jar instruction-decode-command -o ${CA_ROOT_PATH} -i ${CA_ROOT_PATH}/objdumps/${KERNEL}.c -n ${KERNEL} config.yaml

